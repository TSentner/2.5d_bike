%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: BMXMan
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 1
  - m_Path: metarig/hips
    m_Weight: 1
  - m_Path: metarig/hips/spine
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/f_index_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/f_index_01_L/f_index_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/f_index_01_L/f_index_02_L/f_index_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/thumb_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/thumb_01_L/thumb_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_01_L/thumb_01_L/thumb_02_L/thumb_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_02_L/f_middle_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_02_L/f_middle_01_L/f_middle_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_02_L/f_middle_01_L/f_middle_02_L/f_middle_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_03_L/f_ring_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_03_L/f_ring_01_L/f_ring_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_03_L/f_ring_01_L/f_ring_02_L/f_ring_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_04_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_04_L/f_pinky_01_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_04_L/f_pinky_01_L/f_pinky_02_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L/palm_04_L/f_pinky_01_L/f_pinky_02_L/f_pinky_03_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/Bone012
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/Bone012/f_ring_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/Bone012/f_ring_01_R/f_ring_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/Bone012/f_ring_01_R/f_ring_02_R/f_ring_03_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/f_index_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/f_index_01_R/f_index_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/f_index_01_R/f_index_02_R/f_index_03_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/thumb_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/thumb_01_R/thumb_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_01_R/thumb_01_R/thumb_02_R/thumb_03_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_02_R/f_middle_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_02_R/f_middle_01_R/f_middle_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_02_R/f_middle_01_R/f_middle_02_R/f_middle_03_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_04_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_04_R/f_pinky_01_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_04_R/f_pinky_01_R/f_pinky_02_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R/palm_04_R/f_pinky_01_R/f_pinky_02_R/f_pinky_03_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L/foot_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L/foot_L/toe_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R/foot_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R/foot_R/toe_R
    m_Weight: 1
  - m_Path: metarig/main_bone
    m_Weight: 1
  - m_Path: metarig/main_bone/scooter_body
    m_Weight: 1
  - m_Path: metarig/main_bone/scooter_body/control_scooter
    m_Weight: 1
  - m_Path: Null
    m_Weight: 1
  - m_Path: Null 1
    m_Weight: 1
  - m_Path: Null 2
    m_Weight: 1
  - m_Path: Null 3
    m_Weight: 1
  - m_Path: Null 4
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:ChestEndEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:ChestOriginEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:HeadEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:LeftUpLeg
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:LeftUpLeg/ScooterRiderNoHat_Ctrl:LeftLeg
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:LeftUpLeg/ScooterRiderNoHat_Ctrl:LeftLeg/ScooterRiderNoHat_Ctrl:LeftFoot
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:LeftUpLeg/ScooterRiderNoHat_Ctrl:LeftLeg/ScooterRiderNoHat_Ctrl:LeftFoot/ScooterRiderNoHat_Ctrl:LeftToeBase
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:RightUpLeg
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:RightUpLeg/ScooterRiderNoHat_Ctrl:RightLeg
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:RightUpLeg/ScooterRiderNoHat_Ctrl:RightLeg/ScooterRiderNoHat_Ctrl:RightFoot
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:RightUpLeg/ScooterRiderNoHat_Ctrl:RightLeg/ScooterRiderNoHat_Ctrl:RightFoot/ScooterRiderNoHat_Ctrl:RightToeBase
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandIndex1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandIndex1/ScooterRiderNoHat_Ctrl:LeftHandIndex2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandIndex1/ScooterRiderNoHat_Ctrl:LeftHandIndex2/ScooterRiderNoHat_Ctrl:LeftHandIndex3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandMiddle1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandMiddle1/ScooterRiderNoHat_Ctrl:LeftHandMiddle2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandMiddle1/ScooterRiderNoHat_Ctrl:LeftHandMiddle2/ScooterRiderNoHat_Ctrl:LeftHandMiddle3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandPinky1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandPinky1/ScooterRiderNoHat_Ctrl:LeftHandPinky2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandPinky1/ScooterRiderNoHat_Ctrl:LeftHandPinky2/ScooterRiderNoHat_Ctrl:LeftHandPinky3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandRing1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandRing1/ScooterRiderNoHat_Ctrl:LeftHandRing2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandRing1/ScooterRiderNoHat_Ctrl:LeftHandRing2/ScooterRiderNoHat_Ctrl:LeftHandRing3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandThumb1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandThumb1/ScooterRiderNoHat_Ctrl:LeftHandThumb2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandThumb1/ScooterRiderNoHat_Ctrl:LeftHandThumb2/ScooterRiderNoHat_Ctrl:LeftHandThumb3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:LeftShoulder/ScooterRiderNoHat_Ctrl:LeftArm/ScooterRiderNoHat_Ctrl:LeftForeArm/ScooterRiderNoHat_Ctrl:LeftHand/ScooterRiderNoHat_Ctrl:LeftHandThumb1/ScooterRiderNoHat_Ctrl:LeftHandThumb2/ScooterRiderNoHat_Ctrl:LeftHandThumb3/ScooterRiderNoHat_Ctrl:LeftHandThumb4
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:Neck
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:Neck/ScooterRiderNoHat_Ctrl:Head
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandIndex1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandIndex1/ScooterRiderNoHat_Ctrl:RightHandIndex2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandIndex1/ScooterRiderNoHat_Ctrl:RightHandIndex2/ScooterRiderNoHat_Ctrl:RightHandIndex3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandMiddle1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandMiddle1/ScooterRiderNoHat_Ctrl:RightHandMiddle2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandMiddle1/ScooterRiderNoHat_Ctrl:RightHandMiddle2/ScooterRiderNoHat_Ctrl:RightHandMiddle3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandPinky1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandPinky1/ScooterRiderNoHat_Ctrl:RightHandPinky2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandPinky1/ScooterRiderNoHat_Ctrl:RightHandPinky2/ScooterRiderNoHat_Ctrl:RightHandPinky3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandRing1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandRing1/ScooterRiderNoHat_Ctrl:RightHandRing2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandRing1/ScooterRiderNoHat_Ctrl:RightHandRing2/ScooterRiderNoHat_Ctrl:RightHandRing3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandThumb1
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandThumb1/ScooterRiderNoHat_Ctrl:RightHandThumb2
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:Hips/ScooterRiderNoHat_Ctrl:Spine/ScooterRiderNoHat_Ctrl:Spine1/ScooterRiderNoHat_Ctrl:RightShoulder/ScooterRiderNoHat_Ctrl:RightArm/ScooterRiderNoHat_Ctrl:RightForeArm/ScooterRiderNoHat_Ctrl:RightHand/ScooterRiderNoHat_Ctrl:RightHandThumb1/ScooterRiderNoHat_Ctrl:RightHandThumb2/ScooterRiderNoHat_Ctrl:RightHandThumb3
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:HipsEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftAnkleEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftElbowEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftFootEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHandIndexEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHandMiddleEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHandPinkyEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHandRingEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHandThumbEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftHipEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftKneeEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftShoulderEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:LeftWristEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightAnkleEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightElbowEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightFootEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHandIndexEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHandMiddleEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHandPinkyEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHandRingEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHandThumbEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightHipEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightKneeEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightShoulderEffector
    m_Weight: 1
  - m_Path: ScooterRiderNoHat_Ctrl:Reference/ScooterRiderNoHat_Ctrl:RightWristEffector
    m_Weight: 1
