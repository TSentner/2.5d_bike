﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputEventsHandler : MonoBehaviour {
    private bool pressed;
    private bool trickEnd = true;
	private int lastIndex;
	KeyCode[] keycodes = new KeyCode[]
	{
		KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5,
		KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9, KeyCode.Alpha0,
		KeyCode.Q, KeyCode.W, KeyCode.E, KeyCode.R, KeyCode.T,
		KeyCode.Y, KeyCode.U, KeyCode.I, KeyCode.O, KeyCode.P,
		KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.F, KeyCode.G,
		KeyCode.H, KeyCode.J, KeyCode.K, KeyCode.L, KeyCode.Semicolon,
		KeyCode.Z, KeyCode.X, KeyCode.C, KeyCode.V, KeyCode.B,
		KeyCode.N
	};

	private void OnEnable()
	{
		/*TrickJoystik.OnTrickPlayed += ((arg) =>
		{
			PlayTrick(arg);
			pressed = true;
		});
		TrickJoystik.OnTrickEnd += (()=>
		{
            trickEnd = true;
		});*/
	}

    private void OnDisable()
    {
        /*TrickJoystik.OnTrickPlayed -= ((arg) =>
        {
            PlayTrick(arg);
            pressed = true;
        });
		TrickJoystik.OnTrickEnd -= (()=>
		{
            trickEnd = true;
		});*/
    }

	void Update () 
	{
	
			for (int i = 0; i < keycodes.Length; i++)
			{
				if (Input.GetKeyDown(keycodes[i]))
					PlayTrick(i);
				if (Input.GetKeyUp(keycodes[i]))
					trickEnd = true;
				if (Input.GetKey(keycodes[i]))
					pressed = true;

			}

			BMXAnimationSystem.Instance.keypressed = pressed;

			if (pressed && BMXAnimationSystem.Instance.lastIndex != lastIndex)
			{
				trickEnd = true;
				PlayTrick(lastIndex);
			}

			pressed = false;
	
	}

	private void PlayTrick(int index)
	{

		if(trickEnd  && (BMXMainController.Instance.mainBody.velocity.y>0.3f || (!BMXMainController.Instance.OnGround && !BMXMainController.Instance.OnGrind) ))
		{
          BMXAnimationSystem.Instance.MakeTrick(index);
		  trickEnd = false;
		  lastIndex = index;
		}
	}
}
