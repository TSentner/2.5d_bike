﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkRotation : MonoBehaviour 
{
    public Transform TargetFork;
	public Vector3 Offset;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		transform.localEulerAngles = new Vector3(Offset.x,
		Mathf.LerpAngle(transform.localEulerAngles.y, TargetFork.localEulerAngles.y + Offset.y,1), 
		Mathf.LerpAngle(transform.localEulerAngles.z, TargetFork.localEulerAngles.z + Offset.z,1));
	}
}
