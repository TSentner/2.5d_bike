﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    [HideInInspector]public Rigidbody mainBody;
    public string layerMask;

    public bool Cast;
    public float sphereCastRadius = 0.25f;
    public LayerMask mask;

    private void Start()
    {
        mainBody = GetComponent<Rigidbody>();
    }

    private void LateUpdate()
    {
        if (Cast)
        {
           /*
           RaycastHit hit;
           if(Physics.Raycast(transform.position,Vector3.down, out hit, 5, mask))
           {
               if(hit.distance < sphereCastRadius)
               {
                  Collided = true;
               }
           }
           */

           var t = Physics.OverlapSphere(transform.position,sphereCastRadius,mask);
           if(t.Length > 0)
           {
               Collided = true;
           }
        }
    }

    public bool Collided;
    private void OnCollisionStay(Collision other)
    {
        if(other.gameObject.layer != LayerMask.NameToLayer(layerMask))
          return;

        Collided = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.layer != LayerMask.NameToLayer(layerMask))
          return;

          Collided = true;
    }



    private void OnCollisionExit(Collision other)
    {
        if(other.gameObject.layer != LayerMask.NameToLayer(layerMask))
          return;

        Collided = false;
    }
}
