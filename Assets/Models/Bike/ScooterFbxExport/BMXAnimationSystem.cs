﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Random = UnityEngine.Random;

public class BMXAnimationSystem : MonoBehaviour
{
    public static BMXAnimationSystem Instance;

 
 
    [System.Serializable]

    public class TricksAnimations
    {
        public string animationName;
        public bool IKHandR;
        public bool IKHandL;
        public bool IKFootR;
        public bool IKFootL;
    }
    public TricksAnimations[] tricksAnimations;
    public TricksAnimations[] tricksAnimationsScooter;

   public InverseKinematics IKHandR;
    public InverseKinematics IKHandL;
   public InverseKinematics IKFootR;
    public InverseKinematics IKFootL;
    

    public HipPositionController HipPosition;

    public Animator BMXAnimator;
    public bool InTrick;
    private int FBLean;
    private float FBLeanBlend;

    private int UDLean;
    private float UDLeanBlend;

    private float hopeEval;
    private bool fix;
    private string currentLoopClipName;
    private bool loop;
    public bool keypressed;
    public bool trick;
    public int lastIndex;
    public bool UDLeanEnable;
    public bool PushOnGround;
    public bool Accel;
    AnimatorClipInfo[] m_CurrentClipInfo;
    private float CrossFadeTimer;
    private void Awake()
    {
        Instance = this;
    }

    private KeyCode lastpressed;
    public void MakeTrick(int trickN)
    {
        if (BMXMainController.Instance.m_BikeType == BMXMainController.BikeType.Bike)
        {
            if (!BMXMainController.Instance.InTrick && trickN <= tricksAnimations.Length)
            {
                // BMXAnimator.SetFloat("Blend", trickN);
                // BMXAnimator.SetTrigger("MakeTrick");
                lastIndex = trickN;
                trick = true;
                CrossFadeTimer = 0;
            }
        }
        else if (BMXMainController.Instance.m_BikeType == BMXMainController.BikeType.Scooter)
        {
            if (!BMXMainController.Instance.InTrick && trickN <= tricksAnimationsScooter.Length)
            {
                // BMXAnimator.SetFloat("Blend", trickN);
                // BMXAnimator.SetTrigger("MakeTrick");
                lastIndex = trickN;
                trick = true;
                CrossFadeTimer = 0;
            }
        }
    }

    private float LastLanded;
    public void Active(bool force_push = false, bool bunnyhope = false, bool landing = false, bool lean = false, bool stand = false, float lean_coef = 0)
    {
       
       // IKFootR.ActiveByT = true;
        if (BMXMainController.Instance.InTrick)
        {
            if (BMXMainController.Instance.m_BikeType == BMXMainController.BikeType.Bike)
            {
                IKHandR.Active = tricksAnimations[lastIndex].IKHandR;
                IKHandL.Active = tricksAnimations[lastIndex].IKHandL;
                IKFootR.Active = tricksAnimations[lastIndex].IKFootR;
                IKFootL.Active = tricksAnimations[lastIndex].IKFootL;
               // IKFootR.Stand = false;
            }
            else if (BMXMainController.Instance.m_BikeType == BMXMainController.BikeType.Scooter)
            {
                IKHandR.Active = tricksAnimationsScooter[lastIndex].IKHandR;
                IKHandL.Active = tricksAnimationsScooter[lastIndex].IKHandL;
                IKFootR.Active = tricksAnimationsScooter[lastIndex].IKFootR;
                IKFootL.Active = tricksAnimationsScooter[lastIndex].IKFootL;
              //  IKFootR.Stand = false;
            }

            
        }
        else
        {
            IKHandR.Active = true;
            IKHandL.Active = true;
            //IKFootR.Active = !force_push && !stand;
            IKFootR.Active = true; ///////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            IKFootL.Active = true;

            HipPosition.isPartialy = force_push;
            HipPosition.isBunnyHope = bunnyhope;
            HipPosition.isLanding = landing;
            HipPosition.isLean = lean;
            HipPosition.LeanCoef = lean_coef;
            HipPosition.isStand = stand;

                HipPosition.progress = BMXAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }

       
    }

    public void PushOnGroundVoid()
    {

    }
    public string GetCurrentClipName()
    {
        m_CurrentClipInfo = BMXAnimator.GetCurrentAnimatorClipInfo(0);
        return m_CurrentClipInfo[0].clip.name;
    }

    public void Fix()
    {
        fix = true;
    }

    public void Loop(string name)
    {
        loop = true;
        currentLoopClipName = name;
        TrickLoopHolded?.Invoke(lastIndex);
       //  if(ScoreController.Instance!=null && ScoreController.Instance.AddExtensionScoreEventLoop != null)ScoreController.Instance.AddExtensionScoreEventLoop.Invoke(currentLoopClipName);
    }

    public static Action<int> TrickFixHolded;
    public static Action<int> TrickLoopHolded;
    public static Action<int> TrickCompete;

    public void TrickEnded()
    {
        TrickCompete?.Invoke(lastIndex);
    }

    private void OnDisable()
    {

    }
    
    [Range(0, 1)]
    public float time_t;
    void Update()
    {

        var force_push = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff") ||
                         BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff 0");
        bool bunny_hope = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("EndGround");
        bool landing = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("StartGround");
        bool lean = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("LeanTree");
        bool stand = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stand") || 
                     BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Stand 1");

        bool neutral = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("BunnieHopNeutralPosition");

        if (force_push)
        {
            BMXAnimator.Play(BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff") ? "Pushoff" : "Pushoff 0", 0, time_t);
        }

        Active(force_push,bunny_hope, landing, lean,stand, FBLean);
        

        HipPosition.isTrick = !bunny_hope && !landing && !lean && !stand && !force_push && !neutral;
         
         CrossFadeTimer+= Time.deltaTime;
         if(trick && CrossFadeTimer < 0.25f)
         {
             switch (BMXMainController.Instance.m_BikeType)
             {
                 case BMXMainController.BikeType.Bike:
                     BMXAnimator.CrossFade(tricksAnimations[lastIndex].animationName, 0f);
                     break;
                 case BMXMainController.BikeType.Scooter:
                     BMXAnimator.CrossFade(tricksAnimationsScooter[lastIndex].animationName, 0f);
                     break;
             }
         }
        BMXAnimator.SetFloat("PlayerSpeed", BMXMainController.Instance.mainBody.velocity.magnitude);
         
        if (keypressed && fix)
        {
            float f = Mathf.Sin((Time.time * 5)) / 5f;
            BMXAnimator.SetFloat("Speed", f);
            TrickFixHolded?.Invoke(lastIndex);
          //  if(ScoreController.Instance!=null && ScoreController.Instance.AddExtensionScoreEvent != null)ScoreController.Instance.AddExtensionScoreEvent.Invoke(GetCurrentClipName());
        }
        else
        {
            fix = false;
            if(!loop)
            BMXAnimator.SetFloat("Speed", Mathf.Lerp(BMXAnimator.GetFloat("Speed"), 1, 0.1f));
        }

        BMXAnimator.SetBool("TrickLoop",keypressed);

        if (keypressed && loop)
        {
            // currentLoopClipName
            float f = Mathf.Lerp(BMXAnimator.GetFloat("Speed"),2,0.025f);
            BMXAnimator.SetFloat("Speed", f);
         //   if(ScoreController.Instance!=null && ScoreController.Instance.AddExtensionScoreEvent != null)ScoreController.Instance.AddExtensionScoreEvent.Invoke(currentLoopClipName);
            // if(ScoreController.Instance.AddExtensionScoreEvent != null)ScoreController.Instance.AddExtensionScoreEvent.Invoke(currentLoopClipName);
            // ScoreController.AddExtensionScoreEventLoop.Invoke(currentLoopClipName);
        }else{
            loop = false;
            if(!fix)
            BMXAnimator.SetFloat("Speed", Mathf.Lerp(BMXAnimator.GetFloat("Speed"), 1, 0.1f));
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        FBLean = Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f ? (int)Mathf.Sign(Input.GetAxis("Horizontal")) : 0;
        BMXAnimator.SetBool("Accel",Accel);
#else
        FBLean = (Time.timeScale > 0 && Mathf.Abs(Input.acceleration.x)>0.2f)? (int)Mathf.Sign(Input.acceleration.x):0;

      BMXAnimator.SetBool("Accel",Accel);
     //   UDLean = (BMXMainController.Instance.OnJump) ? 1:0;
#endif
        //	UDLean = Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f ? (int)Mathf.Sign(Input.GetAxis("Vertical")) : 0;





        FBLeanBlend = Mathf.Lerp(FBLeanBlend, FBLean, FBLeanCoef);
        if(UDLeanEnable)UDLeanBlend = Mathf.Lerp(UDLeanBlend, UDLean, 0.1f);
        BMXAnimator.SetFloat("FBLeanBlend", FBLeanBlend);
        BMXAnimator.SetInteger("FBLean", FBLean);
       // BMXAnimator.SetFloat("UDLeanBlend", UDLeanBlend);
       // if(UDLeanEnable)BMXAnimator.SetInteger("UDLean", UDLean);
       BMXAnimator.SetBool("OnGround", BMXMainController.Instance.OnGround);
      //  BMXAnimator.SetBool("OnGround", true);
     
       
    }

    public void GroundedEvent()
    {
        if (BMXMainController.Instance.Grounded && Time.time>LastLanded)
        {
            LastLanded = Time.time + 0.9f;
            BMXAnimator.ResetTrigger("Grounded");
            BMXAnimator.SetTrigger("Grounded");

        }
    }


    public float FBLeanCoef;
    public void OnHit()
    {
        BMXAnimator.Play("EndGround");
    }

}
