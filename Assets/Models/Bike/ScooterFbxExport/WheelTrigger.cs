﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelTrigger : MonoBehaviour
{
	public bool isGrounded;
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerStay(Collider other){
//		Debug.Log("Trigger Stay "+ gameObject.name);
		isGrounded = true;
    }
    public void OnTriggerExit(Collider other){
//		Debug.Log("Trigger Exit "+ gameObject.name);
		isGrounded = false;
    }
}
