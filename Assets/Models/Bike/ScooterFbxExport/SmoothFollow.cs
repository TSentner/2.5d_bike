/*
This camera smoothes out rotation around the y-axis and height.
Horizontal Distance to the target is always fixed.

There are many different ways to smooth the rotation but doing it this way gives you a lot of control over how the camera behaves.

For every of those smoothed values we calculate the wanted value and the current value.
Then we smooth it using the Lerp function.
Then we apply the smoothed values to the transform's position.
*/


using UnityEngine;

//TODO need refactoring
using System;
using JetBrains.Annotations;


namespace Motocross.UI.Game.Elements.Location
{
    // Place the script in the Camera-Control group in the component menu
    [AddComponentMenu( "Camera-Control/Smooth Follow C#" )]
    public class SmoothFollow : MonoBehaviour
    {
        public static SmoothFollow Instance;
        // The target we are following
        public Transform target;
        
        private Camera cam;
        public Vector3 startLocalPosition;

        public float ZoomOutDistance;
        
        public float SmoothX;
        public float SmoothY;
        public float SmoothZ;

        
        public float SmoothZoomOut;

        public float ZoomOutCoef;

        public float TargetEulerY;

        public float CurrentVelocity;
        public Rigidbody body;
        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            cam = GetComponentInChildren<Camera>();
            startLocalPosition = cam.transform.localPosition;
            body = target.GetComponent<Rigidbody>();
        }

        void FixedUpdate()
        {
            if( !target )
                return;
//ZoomOutCoef = body.velocity.magnitude/BikeParametersController.Instance.GetValue(ParamerersType.MaxSpeed);

            TargetEulerY -= body.velocity.magnitude*0.07f;
            TargetEulerY += 0.1f;
            TargetEulerY = Mathf.Clamp(TargetEulerY,0, 10);
            Vector3 target_vector = target.position;
            Vector3 current_position = transform.position;
            current_position.x = Mathf.Lerp(current_position.x, target_vector.x, SmoothX);
            current_position.y = Mathf.Lerp(current_position.y, target_vector.y, SmoothY);
            current_position.z = Mathf.Lerp(current_position.z, target_vector.z, SmoothZ);
            transform.position = current_position;

            cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition,startLocalPosition+Vector3.forward*ZoomOutCoef*ZoomOutDistance,SmoothZoomOut );

            var ang = Mathf.LerpAngle(transform.eulerAngles.y, TargetEulerY, 0.02f);
            transform.eulerAngles = new Vector3(
                transform.eulerAngles.x,
                TargetEulerY,
                transform.eulerAngles.z);


        }
    }
}