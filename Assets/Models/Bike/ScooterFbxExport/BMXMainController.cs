using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.CodeDom;

using Motocross.UI.Game.Elements.Location;
using UnityEngine.Experimental.PlayerLoop;
using Random = UnityEngine.Random;

public class BMXMainController : MonoBehaviour
{
	#region old_vars

	public static BMXMainController Instance;

	public enum BikeType
	{
		Bike,
		Scooter
	}

	public BikeType m_BikeType;


	public float turnPower
	{
		get
		{
			return 0;
				//BikeParametersController.Instance.GetValue(ParamerersType
				//	.SpinPower); 
			
			/*BikeParametersController.Instance.Parameters[5].ParameterValue;*/
		}
	}

	public float spinCoef
	{
		get
		{
			return 0;
				/*
				BikeParametersController.Instance.GetValue(ParamerersType
									.SpinAccel); */
				
		}
	}

	 public Vector3 HeadRotateOffset;
	[Header("Parameters")] public float maxSpinSpeed;
	public float crashAngle;
	public float crashAngleZ;
	public float headIKAngle;
	public float badJumpAngle;
	public float SpinHeadAngle;
	[Header("Components")] public Rigidbody mainBody;
	public Transform HeadBone;
	public LayerMask mask;
	public CollisionHandler[] wheelsCol;
	public CollisionHandler[] crahedCol;
	public bool InTrick;
	public bool Crashed;
	[Header("Sound")] public AudioSource source;
	public AudioClip[] onCrashSound;
	public AudioClip[] onGroundSoundStart;
	public AudioClip[] onGroundSoundEnd;
	private bool grounded;
	private float t_SpinCoef = 1;
	private float t_SpinTimer;
	private bool t_SpinActive;
	private bool t_HeadIKActive;

	public Transform TransformGhost;

	///Если активно, то напревление спина будет инвертировано
	private bool t_InvertDirectionSpin;

	[HideInInspector] public bool LastOppoSpin;

	///Отображает последнюю ротацию (degree180/degree360)
	private bool t_LastRotation = true;

	private bool degree180 = true;
	private bool doubleTapD = false;

	///Нажатие на кнопку спина
	private bool OnSpinTap;

	///Удержание кнопки спина
	private bool OnSpinPress;

	private bool audio = true;
	private float lerpedFlipCoef;
	private int JumpCounter;
	private Quaternion startHeadRot;
	private Quaternion headRot;
	private bool spinTutorial;
	private bool spin180Tutorial;
	private bool flipTutorial;
	private bool holdenTutorial;
	private bool oppoSpinTutorial;
	private bool trickTutorial;
	public bool tutorialActive;
	private float LatestRandomTime;
	public bool OnJump { get; set; }

	#endregion

	public static Action<bool, bool> SpinCompleted;
	private void StopSpin()
	{
		
		SpinCompleted?.Invoke(t_InvertDirectionSpin,doubleTapD);
		float LastRot = (GetCrashAngle360(crashAngle)) ? 0 : 180;
		PlayerPrefs.SetFloat("LastRot", LastRot);
		t_LastRotation = (GetCrashAngle360(crashAngle)) ? true : false;
		if (t_InvertDirectionSpin) oppoSpinTutorial = true;
		
		t_InvertDirectionSpin = false;
		t_SpinActive = false;
		t_SpinCoef = 0;
	

		// ScoreController.CancelScoreEvent.Invoke("360",1);
	}
	private void Awake()
	{
		Instance = this;
	}

	private float defaultAngularDrag;
	public float WheellieAngularDrag;
	private float Accel;
	private float Lean;
	private bool Jump;

	private bool Slide;
	private float TargetEulerY, TargetEulerX;
	public bool OnGround;
	public bool Grounded;
	public bool Jumped;
	private bool OnGroundRear, OnGroundFront, OnGroundBoth;
	public bool OnGrind;
	private bool OnGroundedBody;
	private GroundedState prevGroundedState;
	public GroundedState currentGroundedState;
	private Collider[] ScooterColliders;
	public LayerMask Constructor;
	public float LocVelX;
	public float MaxVelX;
	private bool inWheellie;
	private float MoveDownPath;
	private Vector3 prevBodyVel;
	private Vector3 prevBodyLocalVel;
	private Vector3 landingBodyVel;
	public float WhellieLandCrashTrashold;
	private bool MoveStarted;
	public AnimationCurve SlideForceCurve;
	public AnimationCurve SlideJitterCurve;

	public  float DefaultDrag;
	public float SlideDrag = 1;
	private Rigidbody oth;
	public WheelCollider slideWheel;
	public WheelCollider rearWheelCollider;
	public WheelTrigger frontWheel;
	public  WheelTrigger rearWheel;
	public Transform BMXRoot;
	public GameObject SparksParticles;
	private void Start()
	{
		ScooterColliders = GetComponents<Collider>();
		defaultAngularDrag = mainBody.angularDrag;
	 
	//	slideWheel.ConfigureVehicleSubsteps(1,3,4);
		DefaultDrag = mainBody.drag;
		MaxVelX = 0;//BikeParametersController.Instance.GetValue(ParamerersType.MaxSpeed);
		headRot = Quaternion.Euler(HeadBone.localEulerAngles.x, HeadBone.localEulerAngles.y, 0);
			  startHeadRot = HeadBone.localRotation;
			   HeadBone.localEulerAngles = Vector3.zero;

		//mainBody.centerOfMass = Vector3.up * 0.001f;
	}

	public bool isForwardDir;

	private bool MovePressed;
	private bool SpinHolded;
	private void OnEnable()
	{
		/*MoveSpinInput.MoveButtonDown += () => { MovePressed = true;};
		MoveSpinInput.MoveButtonUp += () => { MovePressed = false;
			};

			MoveSpinInput.JumpButtonDown += () => { if (OnGround||OnGrind)JumpVoid();};

		MoveSpinInput.SpinButtonDown += () => { OnSpunButtonDown();
			SpinHolded = true;
		};
		MoveSpinInput.SpinButtonUp += () => { SpinHolded = false;};
		TrickJoystik.OppoSpinTap += () => { TargetEulerY -= 180;  oppositeSpin = true;};*/
	}
	private void OnDisable()
	{
		/*MoveSpinInput.MoveButtonDown = () => { };
		MoveSpinInput.MoveButtonUp = () => { };
		MoveSpinInput.SpinButtonDown = () => { };
		
		MoveSpinInput.SpinButtonUp = () => { };
		TrickJoystik.OppoSpinTap = () => { };      
			MoveSpinInput.JumpButtonDown = () => { };*/
		SparksParticles.active = false;
	}


	void Update()
	{
	 
	 //   Debug.Log(Input.acceleration.x);
		if (currentGroundedState != GroundedState.InAirBoth  ) t_SpinCoef = 0;
		Accel = Input.GetKey(KeyCode.UpArrow) || MovePressed ? 1 : 0;
		Accel = Mathf.Clamp01(Accel);
		if (!MoveStarted && Accel >0.1f && mainBody.velocity.magnitude>=0.4f)
			MoveStarted = true;


		Lean = 0;
		
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE_OSX
		if (Input.GetKey(KeyCode.RightArrow)) Lean += 1;
		if (Input.GetKey(KeyCode.LeftArrow)) Lean -= 1;
#else
		Lean = Input.acceleration.x;
#endif
	   
		
		if (Input.GetKeyDown(KeyCode.RightShift)) TargetEulerY -= 180;
		
		Jump = Input.GetKeyDown(KeyCode.Space) && (OnGround||OnGrind);
		if (Jump)
		{
			JumpVoid();
			Jump = false;
		}

		if (Input.GetKeyDown(KeyCode.RightShift))
		{
			OnSpunButtonDown();
			oppositeSpin = true;
		}

		if (Input.GetKeyDown(KeyCode.LeftShift))
		{
			OnSpunButtonDown();
		}

		OnSpinPress = (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) || SpinHolded;
		
		
		bool crashedCollision = crahedCol.Any(x => x.Collided);
		if (!Crashed && ((OnGround||OnGrind) && (InTrick || t_SpinActive)) || crashedCollision)
		{
			Debug.Log(" if (!Crashed && (OnGround && (InTrick || t_SpinActive)) || crashedCollision)");
			StartCoroutine(Crash());
		}

		
	}

	void JumpVoid()
	{
		mainBody.AddForce(
			Vector3.up); /*
			*
						BikeParametersController.Instance.GetValue(ParamerersType
							.JumpPower))*/
			;
		if (OnGrind)
		{
			BMXAnimationSystem.Instance.BMXAnimator.SetBool("JumpB", true);
			BMXAnimationSystem.Instance.BMXAnimator.SetTrigger("Jump");           
			Invoke("InvokedReset", 0.15f);
		}
	}

	void JumpEvent()
	{
		mainBody.SetClampLocalAngularVelZ(0);
		mainBody.angularDrag = defaultAngularDrag;
		if (Vector3.Angle(transform.up, Vector3.up) < 50 && mainBody.velocity.y > 0.01f)
		{
			BMXAnimationSystem.Instance.BMXAnimator.SetBool("JumpB", true);
			BMXAnimationSystem.Instance.BMXAnimator.SetTrigger("Jump");           
			Invoke("InvokedReset", 0.15f);
		}

		
		
			
		
	}

	public static Action GroundedAction;
	void GroundedEvent()
	{
		Debug.Log("gr");
		BMXAnimationSystem.Instance.GroundedEvent();
		GroundedAction?.Invoke();
		ForwardFlipCounter = 0;
		BackFlipCounter = 0;
		SpinCounter = 0;
		//if (ScoreController.Instance!=null && ScoreController.Instance.CanselTricksEvent != null) ScoreController.Instance.CanselTricksEvent.Invoke();
	}

	void InvokedReset()
	{
		BMXAnimationSystem.Instance.BMXAnimator.ResetTrigger("Jump");
		BMXAnimationSystem.Instance.BMXAnimator.SetBool("JumpB", false);
	}

	private bool SlideAreaBellow;
	private float distanceToGrind;
	private float prevDistanceToGrind;
	public float SlideForcePower = 100;
	public float Damper = -1;

	float GetAngle(Vector3 one, Vector3 two)
	{
		int sign = Vector3.Cross(one, two).z < 0 ? -1 : 1;
		return  (sign * Vector3.Angle(one, two));
	}

	private List<bool> grindBuffer = new List<bool>();
	private int iters;
	private bool prevOnGrind;
	void FixedUpdate()
	{
		CheckFlip();
	//	rearWheelCollider.enabled = !inWheellie;
		CheckGrounded();
		OnGround = OnGroundRear || OnGroundFront || OnGroundedBody;

		//OnGrind = slideWheel.isGrounded;
		SparksParticles.active = OnGrind;

		if (Physics.Raycast(new Ray(transform.position, -transform.up), out var hit, .7f, Constructor)  && hit.collider.CompareTag("Slide"))
		{
			SlideAreaBellow = true;
			Debug.DrawLine(hit.point,hit.point+hit.normal);
			if(mainBody.velocity.y<-0.01f)
				mainBody.drag = DefaultDrag;
			else
				mainBody.drag = SlideDrag;
			
			if (Vector3.Angle(hit.normal, transform.up) > 40) StartCoroutine(Crash());
		}
		else
		{
			SlideAreaBellow = false;
			mainBody.drag = DefaultDrag;
		}

		var UpAngle = GetAngle(transform.up, Vector3.up);
		
		BMXAnimationSystem.Instance.HipPosition.SetHillParams(
			Mathf.Abs(UpAngle) > 5 && OnGroundBoth, UpAngle);
	
		if(inWheellie && OnGround && mainBody.velocity.magnitude<=1.5f)
		   StartCoroutine(Crash());
	   if(Slide&&mainBody.velocity.magnitude<=1.5f)
		  StartCoroutine(Crash());

		isForwardDir = Vector3.Angle(transform.right, Vector3.right) < 70;
		//!(mainBody.velocity.magnitude>0.1f) || Vector3.Angle(transform.right, mainBody.velocity.normalized) < 5f;

		LocVelX = mainBody.GetLocalVelocity().x;
		Jumped = false;
		if (prevGroundedState != GroundedState.InAirBoth && currentGroundedState == GroundedState.InAirBoth) // Отрыв
			JumpEvent();

		Slide = SlideAreaBellow; //Чекаем если слайд
		if (Slide)
		{
			
			TargetEulerY = 40;

			mainBody.SetLocalAngularVelZ(0);
		}
		else
			TargetEulerY = 0;

		FixTransform(); // Чёб не сыбался
		float FlipInput = 0;
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE_OSX
	   FlipInput = Input.GetAxis("Horizontal");
#else
		FlipInput = Input.acceleration.x*2f;
#endif

		lerpedFlipCoef = 0;/*
		Mathf.Lerp(lerpedFlipCoef, FlipInput,
					BikeParametersController.Instance.GetValue(ParamerersType.InAirRotationAccel));*/
		
		if (!OnGround && !OnGrind)

		{
			mainBody.transform.Rotate(-Vector3.forward,
				(lerpedFlipCoef */* BikeParametersController.Instance.GetValue(ParamerersType.InAirRotationSpeed) */0*
				 Time.deltaTime) * 1.95f, Space.World);
		}
		else
			lerpedFlipCoef = 0;

		if (prevGroundedState == GroundedState.InAirBoth && (currentGroundedState == GroundedState.RearOnGround ||
															 currentGroundedState == GroundedState.FrontOnGround))
		{
			/*
			if (MoveDownPath < BikeParametersController.Instance.GetValue(ParamerersType.MaxHeightManual) * 100)
						{
							if (Vector3.Angle(Vector3.up, transform.up) > 15)
							{
								inWheellie = true;
								mainBody.SetClampLocalAngularVelZ(0.05f);
								mainBody.angularDrag = WheellieAngularDrag;
							}
						}*/

		 {
				if (Vector3.Angle(Vector3.up, transform.up) > 15)
				{
	
				  
					if (Mathf.Abs(landingBodyVel.y) > 12)
					{

						StartCoroutine(Crash());
					}
				}
			}
		}
		landingBodyVel = mainBody.velocity - prevBodyVel;
		prevBodyVel = mainBody.velocity;

		if (currentGroundedState == GroundedState.BothOnGround &&
			(prevGroundedState == GroundedState.RearOnGround || prevGroundedState == GroundedState.FrontOnGround))
		{
			mainBody.angularDrag = defaultAngularDrag;
			inWheellie = false;
			mainBody.SetLocalAngularVelZ(0);
		}
	  
		if (inWheellie && currentGroundedState != GroundedState.BothOnGround &&
			(currentGroundedState == GroundedState.RearOnGround ||
			 currentGroundedState == GroundedState.FrontOnGround)  )
		{
			var vel = mainBody.GetLocalAngularVelocity().z;
			if (Mathf.Abs(vel) < .4f)
			{

			}

			if (Vector3.Angle(Vector3.up, transform.up) > 15f)
			{
		
			}
		}


        /*BMXAnimationSystem.Instance.Accel = Mathf.Abs(Lean) < 0.2f && Accel > 0.01f &&
											currentGroundedState != GroundedState.InAirBoth
											&& isForwardDir && (LocVelX) < MaxVelX * 0.97f;*/
        BMXAnimationSystem.Instance.Accel = true; ///////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        

        /*
            if ((BMXAnimationSystem.Instance.PushOnGround) &&  currentGroundedState != GroundedState.InAirBoth ) //Толкан
                {mainBody.AddForce(transform.right*(BikeParametersController.Instance.GetValue
                                                         (ParamerersType.Accelleration) *
                                                     (3f * (1 - Mathf.Clamp01(LocVelX /
                                                                                    BikeParametersController.Instance.GetValue
                                                                                        (ParamerersType.MaxSpeed))))));

                }
        */

        var loc_vel = mainBody.GetLocalVelocity();
		
		var current_delta =loc_vel - prevBodyLocalVel;
		prevBodyLocalVel =  mainBody.GetLocalVelocity();
		Spin(SlideAreaBellow?(Vector3.Cross(Vector3.up,hit.normal).z>0?Vector3.Angle(Vector3.up,hit.normal)
			:-Vector3.Angle(Vector3.up,hit.normal)):0);

		if (currentGroundedState == GroundedState.BothOnGround || currentGroundedState == GroundedState.InAirBoth)
			inWheellie = false;
		if (mainBody.velocity.y <= 0 && currentGroundedState == GroundedState.InAirBoth)
			MoveDownPath += Mathf.Abs(mainBody.velocity.y);
		if (currentGroundedState == GroundedState.BothOnGround || currentGroundedState == GroundedState.RearOnGround ||
			currentGroundedState == GroundedState.FrontOnGround)
			MoveDownPath = 0;
		prevGroundedState = currentGroundedState; //задаем предыдущий стейт и обнуляем булевы
	 
		currentGroundedState = GroundedState.InAirBoth;
		OnGroundRear = OnGroundFront = OnGroundBoth = OnGroundedBody = Grounded =Jumped= false;	


	}

	

	private void FixTransform()
	{
	 //  mainBody.SetClampLocalVelX(BikeParametersController.Instance.GetValue(ParamerersType.MaxInertiaSpeed));  	
	}

	private float flipDelta;
	private float latestFlipAngle;
	private bool halfForwardFlip;
	private bool halfBackFlip;
	public int ForwardFlipCounter;
	public int BackFlipCounter;
	public static Action<bool> FlipCompleted; 
	void CheckFlip()
	{
		var signed_angle = Vector3.SignedAngle(transform.up, Vector3.up, Vector3.forward);
		if (signed_angle < 0) signed_angle += 360;
		flipDelta = (signed_angle - latestFlipAngle);		
		latestFlipAngle = signed_angle;
		flipDelta = Mathf.Sign(flipDelta);
		if ((Vector3.Angle(transform.up, Vector3.up)) > 170)
		{
			if (flipDelta == 1) 
				halfForwardFlip = !halfBackFlip;
			if (flipDelta == -1) 
				halfBackFlip = !halfForwardFlip;		
		}
		if ((Vector3.Angle(transform.up, Vector3.up)) < 40)
		{
			if ((halfForwardFlip && flipDelta == 1)||(halfBackFlip && flipDelta == -1))
			{
				if (flipDelta == 1)
				{
				
					string[] ScoreEvent = {"Frontflip","DoubleFrontflip","TripleFrontflip","QuadrupleFrontflip"};
					//if (ScoreController.Instance != null && ScoreController.Instance.AddScoreEvent != null) 
						//ScoreController.Instance.AddScoreEvent.Invoke(ScoreEvent[ForwardFlipCounter]);
					ForwardFlipCounter++;
				}
				else
				{
					
					string[] ScoreEvent = new string[]
					{
						"Backflip","DoubleBackflip","TripleBackflip","QuadrupleBackflip"
					};
					//if (ScoreController.Instance != null && ScoreController.Instance.AddScoreEvent != null) ScoreController.Instance.AddScoreEvent.Invoke(ScoreEvent[BackFlipCounter]);
					BackFlipCounter++;
				}
				
				FlipCompleted?.Invoke(flipDelta==1);
				halfForwardFlip = halfBackFlip = false;
			}
			if ((halfForwardFlip && flipDelta == -1)||(halfBackFlip && flipDelta == 1))
			halfForwardFlip = halfBackFlip = false;
			
		}

		
		
	}

	private void CheckGrounded()
	{
        //if (rearWheel.isGrounded)
        OnGroundRear = true;
        //if (frontWheel.isGrounded)
        OnGroundFront = true;

        OnGroundBoth = OnGroundRear && OnGroundFront;
		if (OnGroundBoth)
			currentGroundedState = GroundedState.BothOnGround;
		else if (OnGroundRear)
			currentGroundedState = GroundedState.RearOnGround;
		else if (OnGroundFront) currentGroundedState = GroundedState.FrontOnGround;
		if (prevGroundedState == GroundedState.InAirBoth && (currentGroundedState == GroundedState.FrontOnGround ||
															 currentGroundedState == GroundedState.RearOnGround||currentGroundedState == GroundedState.BothOnGround))
		{
			Grounded = true;
			GroundedEvent();

		}
		
	}

	private void LateUpdate()
	{
		IKHead();
	}

	private void IKHead()
	{
		headRot = Quaternion.Lerp(headRot, (t_HeadIKActive) ?
		Quaternion.Euler(startHeadRot.x, startHeadRot.y, startHeadRot.z + (t_InvertDirectionSpin ? -SpinHeadAngle : SpinHeadAngle))
		: startHeadRot, 0.075f);

	   HeadBone.localRotation = Quaternion.Euler(HeadBone.localRotation.x + HeadRotateOffset.x, 
		   HeadBone.localRotation.y + HeadRotateOffset.y, 
		   headRot.eulerAngles.z + HeadRotateOffset.z);
		//HeadBone.localRotation = Quaternion.Lerp(HeadBone.localRotation,headRot,0.5f);
	}



	public void OnSpunButtonDown()
	{
		OnSpinTap = true;
	}



	private bool SpinExtension = false;
	private bool oppositeSpin;

	private void Spin(float angle)
	{
		if ( /*Input.GetKeyDown(KeyCode.Space) ||*/ OnSpinTap && !OnGround && !OnGrind)
		{
			OnSpinTap = false;
			// Debug.Log("jopa1");
			if (!t_SpinActive)
			{
				// Debug.Log("jopa2");
				if ( /*Input.GetKey(KeyCode.Space) || */oppositeSpin)
				{
					t_InvertDirectionSpin = true;
					LastOppoSpin = true;
				}

				doubleTapD = false;
				degree180 = false;
				StartCoroutine(DoubleTap());
				if (GetCrashAngle180(crashAngle)) degree180 = true;
				t_HeadIKActive = true;
				t_SpinActive = true;
				t_SpinTimer = 0;
				t_SpinCoef = 1;
			}
		}

		if (t_SpinActive)
		{
			if ( /*Input.GetKey(KeyCode.Space) || */OnSpinPress) t_SpinCoef += Time.deltaTime * spinCoef;

			// Debug.Log(t_SpinCoef);
			t_SpinCoef = Mathf.Clamp(t_SpinCoef, 0, maxSpinSpeed);
			mainBody.transform.Rotate((t_InvertDirectionSpin ? -Vector3.up : Vector3.up),
				turnPower * t_SpinCoef * Time.deltaTime, Space.Self);
			// mainBody.transform.Rotate((t_InvertDirectionSpin ? -Vector3.up : Vector3.up) * turnPower * t_SpinCoef, Space.Self);
			t_SpinTimer += Time.deltaTime;
			/*
			if (ScoreController.Instance != null && ScoreController.Instance.AddExtensionScoreEvent != null)
						{
							ScoreController.Instance.AddExtensionScoreEvent.Invoke("360");
							ScoreController.Instance.AddExtensionScoreEvent.Invoke("180");
							ScoreController.Instance.AddExtensionScoreEvent.Invoke("540");
							ScoreController.Instance.AddExtensionScoreEvent.Invoke("720");
						}*/
			

			if ( /*!Input.GetKey(KeyCode.Space) && */!OnSpinPress && t_SpinTimer > 0.5f)
			{
				if (degree180 && doubleTapD)
				{
					if (GetCrashAngle360(crashAngle))
					{
						StopSpin();
						spin180Tutorial = true;
						if (oppositeSpin) oppositeSpin = false;
					}

					if (GetCrashAngle360(headIKAngle)) StopHeadIK();
				}
				else if (!degree180 && doubleTapD)
				{
					if (GetCrashAngle180(crashAngle))
					{
						StopSpin();
						spin180Tutorial = true;
						if (oppositeSpin) oppositeSpin = false;
					}

				   if (GetCrashAngle180(headIKAngle)) StopHeadIK();
				}
				else if (degree180 && !doubleTapD)
				{
					if (GetCrashAngle180(crashAngle))
					{
						StopSpin();
						spinTutorial = true;
						if (oppositeSpin) oppositeSpin = false;
					}

				   if (GetCrashAngle180(headIKAngle)) StopHeadIK();
				}
				else if (!degree180 && !doubleTapD)
				{
					if (GetCrashAngle360(crashAngle))
					{
						StopSpin();
						spinTutorial = true;
						if (oppositeSpin) oppositeSpin = false;
					}

					if (GetCrashAngle360(headIKAngle)) StopHeadIK();
				}
			}

			if (t_SpinTimer > 0.5f && ( /*Input.GetKey(KeyCode.Space) ||*/ OnSpinPress))
			{
				if (degree180 && doubleTapD)
				{
					if (GetCrashAngle360(60))
						StopSpinEx();
					else
						SpinExtension = false;
				}
				else if (!degree180 && doubleTapD)
				{
					if (GetCrashAngle180(60))
						StopSpinEx();
					else
						SpinExtension = false;
				}
				else if (degree180 && !doubleTapD)
				{
					if (GetCrashAngle180(60))
						StopSpinEx();
					else
						SpinExtension = false;
				}
				else if (!degree180 && !doubleTapD)
				{
					if (GetCrashAngle360(60))

						StopSpinEx();
					else
						SpinExtension = false;
				}
			}
			else
			{
				SpinExtension = false;
			}
		}

		///Если спин недокручен, возвращаем байк в изначальное положение
		/*if (!t_SpinActive) ////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		{
			if (SlideAreaBellow)
			{

				mainBody.transform.localEulerAngles = new Vector3(0,
					Mathf.LerpAngle(mainBody.transform.eulerAngles.y, (t_LastRotation ? 0 : 180),
						0.2f), Mathf.LerpAngle(mainBody.transform.eulerAngles.z,(t_LastRotation ? angle : -angle),0.2f));

				BMXRoot.SetLocalEulersY(Mathf.LerpAngle(BMXRoot.localEulerAngles.y, 130,0.3f));
			   BMXRoot.SetLocalEulersX(Mathf.LerpAngle(BMXRoot.localEulerAngles.x, SlideJitterCurve.Evaluate(Time.time),0.3f));

			}
			else
			{
				BMXRoot.SetLocalEulersY(Mathf.LerpAngle(BMXRoot.localEulerAngles.y, 90,0.1f));
				BMXRoot.SetLocalEulersX(Mathf.LerpAngle(BMXRoot.localEulerAngles.x, -6.67f,0.3f));
				mainBody.transform.eulerAngles = new Vector3(0,
				Mathf.LerpAngle(mainBody.transform.eulerAngles.y, (t_LastRotation ? 0 : 180),
					0.3f),mainBody.transform.eulerAngles.z);
			}

		}*/
	}

	public static Action SpinCounterIncrease;
	public int SpinCounter;
	private void StopSpinEx()
	{
		if (!SpinExtension)
		{
			SpinExtension = true;
			holdenTutorial = true;
//			Debug.Log("Additional");
			SpinCounterIncrease?.Invoke();
			SpinCounter += 2;
		//	if (ScoreController.Instance != null && ScoreController.Instance.AddScoreEvent != null) ScoreController.Instance.AddScoreEvent.Invoke((180 * SpinCounter).ToString());
		}
	}

	IEnumerator DoubleTap()
	{
		float _doubleTapTimeD = 0;
		int TapCounter = 0;
		bool endTime = false;
		yield return new WaitForFixedUpdate();
		if (t_SpinActive)
			while (!endTime)
			{
				_doubleTapTimeD += Time.deltaTime;
				if ( /*Input.GetKeyDown(KeyCode.Space) || */OnSpinTap)
				{
					OnSpinTap = false;
					TapCounter++;
				}

				if (TapCounter >= 1)
				{
					doubleTapD = true;
					TapCounter = 0;
				}

				if (doubleTapD)
				{
					endTime = true;
					SpinCounter++;
					//if (ScoreController.Instance != null && ScoreController.Instance.AddScoreEvent != null) ScoreController.Instance.AddScoreEvent.Invoke((180 * SpinCounter).ToString());
					
				}

				if (_doubleTapTimeD >= 0.3f)
				{
					endTime = true;
					SpinCounter += 2;
				//	if (ScoreController.Instance != null && ScoreController.Instance.AddScoreEvent != null) ScoreController.Instance.AddScoreEvent.Invoke((180 * SpinCounter).ToString());
			
				}

				yield return null;
			}

		yield return null;
	}

	private void StopHeadIK()
	{
		t_HeadIKActive = false;
	}


	private bool GetCrashAngle360(float angle)
	{
		if (mainBody.transform.localEulerAngles.y < angle || mainBody.transform.localEulerAngles.y > 360 - angle)
			if (mainBody.transform.localEulerAngles.x < angle || mainBody.transform.localEulerAngles.x > 360 - angle)
			{
				return true;
			}

		return false;
	}

	private bool GetCrashAngle180(float angle)
	{
		if (mainBody.transform.localEulerAngles.y > 180 - angle && mainBody.transform.localEulerAngles.y < 180 + angle)
			if (mainBody.transform.localEulerAngles.x < angle || mainBody.transform.localEulerAngles.x > 360 - angle)
			{
				return true;
			}

		return false;
	}

	public IEnumerator Crash()
	{
		var all_rbs = BMXAnimationSystem.Instance.BMXAnimator.gameObject.GetComponentsInChildren<Rigidbody>();

		var Fork = GetComponentInChildren<ForkRotation>();
		Fork.enabled = false;
		// ScooterColliders.All(x => x.enabled = false);
		foreach (var cols in ScooterColliders)
		{
			cols.enabled = false;
		}

		InverseKinematics[] ik = GetComponentsInChildren<InverseKinematics>();
		foreach (var i in ik)
		{
			i.enabled = false;
		}
		customIK[] iks = GetComponentsInChildren<customIK>();
		foreach (var i in iks)
		{
			i.enabled = false;
		}
		HipPositionController[] hips = GetComponentsInChildren<HipPositionController>();
		foreach (var i in hips)
		{
			i.enabled = false;
		}

		foreach (var allRb in all_rbs)
		{
			allRb.isKinematic = false;
			allRb.velocity = mainBody.velocity * 0.5f;
			allRb.angularVelocity = Vector3.ClampMagnitude(mainBody.angularVelocity, 1);
		 
		}

		mainBody.velocity = Vector3.zero;
		mainBody.useGravity = false;
		mainBody.drag = 25;
		mainBody.angularDrag = 25;
		var all_c = BMXAnimationSystem.Instance.BMXAnimator.gameObject.GetComponentsInChildren<Collider>();
		foreach (var collider1 in all_c)
		{
			collider1.enabled = true;
		}

		yield return new WaitForEndOfFrame();
		Crashed = true;
		BMXAnimationSystem.Instance.BMXAnimator.enabled = false;

		this.enabled = false;
	}

	private void OnDrawGizmos()
	{
		if(!mainBody.isKinematic)
		    Gizmos.DrawSphere(transform.TransformPoint(mainBody.centerOfMass), 0.05f);
	}
}

public enum GroundedState
{
	None,
	RearOnGround,
	FrontOnGround,
	InAirBoth,
	BothOnGround
}
  