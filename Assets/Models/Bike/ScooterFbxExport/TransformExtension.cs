using System.Collections.Generic;
using UnityEngine;
public static class TransformExtension
{
	public static Vector3 LerpAngle(this Vector3 _source, Vector3 target, float t)
	{
		_source.x = Mathf.LerpAngle(_source.x, target.x, t);
		_source.y = Mathf.LerpAngle(_source.y, target.y, t);
		_source.z = Mathf.LerpAngle(_source.z, target.z, t);
		return _source;
	}
	


	public static void FreezeYZ(this Transform tr, float angle_y)
    {
        Vector3 t_vector = Vector3.zero;
        if (((int)tr.localEulerAngles.x >= 0 && (int)tr.localEulerAngles.x <= 90) ||
        ((int)tr.localEulerAngles.x < 360 && (int)tr.localEulerAngles.x > 270))
        {
            t_vector.z = 0;
            t_vector.y = angle_y;
        }
        else
        {
            t_vector.z = 180;
            t_vector.y = 180 - angle_y;
        }

        t_vector.x = tr.localEulerAngles.x;
        tr.localEulerAngles = t_vector;

    }

    public static void SetLocalEulersX(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(val, tr.localEulerAngles.y, tr.localEulerAngles.z);
    }

    public static void SetLocalEulersY(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(tr.localEulerAngles.x, val, tr.localEulerAngles.z);
    }
    public static void SetLocalEulersZ(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y, val);
    }

    public static void SetEulersX(this Transform tr, float val)
    {
        tr.eulerAngles = new Vector3(val, tr.eulerAngles.y, tr.eulerAngles.z);
    }

    public static void SetEulersY(this Transform tr, float val)
    {
        tr.eulerAngles = new Vector3(tr.eulerAngles.x, val, tr.eulerAngles.z);
    }
    public static void SetEulersZ(this Transform tr, float val)
    {
        tr.eulerAngles = new Vector3(tr.eulerAngles.x, tr.eulerAngles.y, val);
    }

    public static void SetPositionX(this Transform tr, float val)
    {
        tr.position = new Vector3(val, tr.position.y, tr.position.z);
    }

    public static void SetPositionY(this Transform tr, float val)
    {
        tr.position = new Vector3(tr.position.x, val, tr.position.z);
    }

    public static void SetPositionZ(this Transform tr, float val)
    {
        tr.position = new Vector3(tr.position.x, tr.position.y, val);
    }
	public static void SetLocalPositionZ(this Transform tr, float val)
	{
		tr.localPosition = new Vector3(tr.localPosition.x, tr.localPosition.y, val);
	}

    public static void ClampLocalX(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(tr.localEulerAngles.x.ClampAngle(-val, val), tr.localEulerAngles.y, tr.localEulerAngles.z);
    }

    public static void ClampLocalY(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y.ClampAngle(-val, val), tr.localEulerAngles.z);
    }

    public static void SetClampLocalEulersZ(this Transform tr, float val)
    {
        tr.localEulerAngles = new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y, tr.localEulerAngles.z.ClampAngle(-val, val));
    }

    public static float ClampAngle(this
    float currentValue,
    float minAngle,
    float maxAngle,
    float clampAroundAngle = 0
                            )
    {
        float angle = currentValue - (clampAroundAngle + 180);

        while (angle < 0)
        {
            angle += 360;
        }

        angle = Mathf.Repeat(angle, 360);

        return Mathf.Clamp(
            angle - 180,
            minAngle,
            maxAngle
        ) + 360 + clampAroundAngle;
    }


}


public static class RigidbodyExtension
{
	public static Vector3 GetLocalVelocity(this Rigidbody _rb)
	{
		return _rb.transform.InverseTransformVector(_rb.velocity);
	}
	
	public static Vector3 GetLocalAngularVelocity(this Rigidbody _rb)
	{
		return _rb.transform.InverseTransformVector(_rb.angularVelocity);
	}
	
	public static void SetLocalVelocity(this Rigidbody _rb, Vector3 val)
	{
		_rb.velocity = _rb.transform.TransformDirection(val);
	}
	
	public static void SetLocalVelocityX(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.velocity);
		loc_vel.x = val;
		_rb.velocity = _rb.transform.TransformVector(loc_vel);
	}
	public static void SetVelocityY(this Rigidbody _rb, float val)
	{
		var t = _rb.velocity;
		t.y = val;
		_rb.velocity = t;
	}
	
	public static void SetLocalAngularVelX(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.angularVelocity);
		loc_vel.x = val;
		_rb.angularVelocity = _rb.transform.TransformVector(loc_vel);
	}
	public static void SetClampLocalVelZ(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.velocity);
		loc_vel.z = Mathf.Clamp(loc_vel.z, -val, val);
		_rb.velocity = _rb.transform.TransformVector(loc_vel);
	}
	public static void SetClampLocalVelY(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.velocity);
		loc_vel.y = Mathf.Clamp(loc_vel.y, -val, val);
		_rb.velocity = _rb.transform.TransformVector(loc_vel);
	}
	public static void SetClampLocalVelX(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.velocity);
		loc_vel.x = Mathf.Clamp(loc_vel.x, -val, val);
		_rb.velocity = _rb.transform.TransformVector(loc_vel);
	}
	public static void SetClampLocalAngularVelX(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformDirection(_rb.angularVelocity);
		loc_vel.x = Mathf.Clamp(loc_vel.x, -val, val);
		_rb.angularVelocity = _rb.transform.TransformDirection(loc_vel);
	}
	
	public static void SetClampLocalAngularVelZ(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.angularVelocity);
		loc_vel.z = Mathf.Clamp(loc_vel.z, -val, val);
		_rb.angularVelocity = _rb.transform.TransformVector(loc_vel);
	}
	
	public static void SetClampLocalAngularVelY(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.angularVelocity);
		loc_vel.y = Mathf.Clamp(loc_vel.y, -val, val);
		_rb.angularVelocity = _rb.transform.TransformVector(loc_vel);
	}
	
	public static void SetClampAngularVelY(this Rigidbody _rb, float val)
	{
		var loc_vel = (_rb.angularVelocity);
		loc_vel.y = Mathf.Clamp(loc_vel.y,-val,val);
		_rb.angularVelocity = loc_vel;
	}
	
	public static void SetLocalAngularVelY(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.angularVelocity);
		loc_vel.y = val;
		_rb.angularVelocity = _rb.transform.TransformVector(loc_vel);
	}
	
	public static void SetLocalAngularVelZ(this Rigidbody _rb, float val)
	{
		var loc_vel = _rb.transform.InverseTransformVector(_rb.angularVelocity);
		loc_vel.z = val;
		_rb.angularVelocity = _rb.transform.TransformVector(loc_vel);
	}
}

public static class GameObjectExtension
{
	public static Mesh CombineMeshes(this GameObject aGo)
	{
		MeshFilter[] filters = aGo.GetComponentsInChildren<MeshFilter> ();
		List<CombineInstance> combine = new List<CombineInstance> ();
 
		for (int i = 0; i < filters.Length; i++)
		{
			// skip the empty parent GO
			if (filters[i].sharedMesh == null)
				continue;
 
			// combine submeshes
			for (int j = 0; j < filters[i].sharedMesh.subMeshCount; j++)
			{
				CombineInstance ci = new CombineInstance ();
 
				ci.mesh = filters[i].sharedMesh;
				ci.subMeshIndex = j;
				ci.transform = filters[i].transform.localToWorldMatrix;
 
				combine.Add (ci);
			}                
 
		}
 
		Mesh t = new Mesh();
		t.CombineMeshes (combine.ToArray (), true, true);
		return t;
	}

/*	public static Mesh CombineMeshes(this GameObject aGo) {
	MeshRenderer[] meshRenderers = aGo.GetComponentsInChildren<MeshRenderer>(false);
	int totalVertexCount = 0;
	int totalMeshCount = 0;

	if(meshRenderers != null && meshRenderers.Length > 0) {
		foreach(MeshRenderer meshRenderer in meshRenderers) {
			MeshFilter filter = meshRenderer.gameObject.GetComponent<MeshFilter>();
			if(filter != null && filter.sharedMesh != null) {
				totalVertexCount += filter.sharedMesh.vertexCount;
				totalMeshCount++;
			}
		}
	}

	if(totalMeshCount == 0) {
		Debug.Log("No meshes found in children. There's nothing to combine.");
		return null;
	}
	if(totalMeshCount == 1) {
		Debug.Log("Only 1 mesh found in children. There's nothing to combine.");
		return null;
	}
	if(totalVertexCount > 65535) {
		Debug.Log("There are too many vertices to combine into 1 mesh ("+totalVertexCount+"). The max. limit is 65535");
		return null;
	}

	Mesh mesh = new Mesh();
	Matrix4x4 myTransform = aGo.transform.worldToLocalMatrix;
	List<Vector3> vertices = new List<Vector3>();
	List<Vector3> normals = new List<Vector3>();
	List<Vector2> uv1s = new List<Vector2>();
	List<Vector2> uv2s = new List<Vector2>();
	Dictionary<Material, List<int>> subMeshes = new Dictionary<Material, List<int>>();
		//MergeMeshInto(filter.sharedMesh, meshRenderer.sharedMaterials, myTransform * filter.transform.localToWorldMatrix, vertices, normals, uv1s, uv2s, null, subMeshes);
	if(meshRenderers != null && meshRenderers.Length > 0) {
		foreach(MeshRenderer meshRenderer in meshRenderers) {
			MeshFilter filter = meshRenderer.gameObject.GetComponent<MeshFilter>();
			if(filter != null && filter.sharedMesh != null)
			{
				MergeMeshInto(filter.sharedMesh, meshRenderer.sharedMaterials,
					myTransform * filter.transform.localToWorldMatrix, vertices, normals, uv1s, uv2s, subMeshes);
				//MergeMeshInto(filter.sharedMesh, meshRenderer.sharedMaterials, myTransform * filter.transform.localToWorldMatrix, vertices, normals, uv1s, uv2s, null, subMeshes);
	
				if(filter.gameObject != aGo) {
					filter.gameObject.SetActive(false);
				}
			}
		}
	}

	mesh.vertices = vertices.ToArray();
	if(normals.Count>0) mesh.normals = normals.ToArray();
	if(uv1s.Count>0) mesh.uv = uv1s.ToArray();
	if(uv2s.Count>0) mesh.uv2 = uv2s.ToArray();
	mesh.subMeshCount = subMeshes.Keys.Count;
	Material[] materials = new Material[subMeshes.Keys.Count];
	int mIdx = 0;
	foreach(Material m in subMeshes.Keys) {
		materials[mIdx] = m;
		mesh.SetTriangles(subMeshes[m].ToArray(), mIdx++);
	}

	if(meshRenderers != null && meshRenderers.Length > 0) {
		MeshRenderer meshRend = aGo.GetComponent<MeshRenderer>();
		if(meshRend == null) meshRend = aGo.AddComponent<MeshRenderer>();
		meshRend.sharedMaterials = materials;

		MeshFilter meshFilter = aGo.GetComponent<MeshFilter>();
		if(meshFilter == null) meshFilter = aGo.AddComponent<MeshFilter>();
		meshFilter.sharedMesh = mesh;
	}
	return mesh;
}

private static void MergeMeshInto(
Mesh meshToMerge, 
Material[] ms,
Matrix4x4 transformMatrix, 
List<Vector3> vertices, 
List<Vector3> normals, 
List<Vector2> uv1s, 
List<Vector2> uv2s, 
Dictionary<Material, 
List<int>> subMeshes) {
	if(meshToMerge == null) return;
	int vertexOffset = vertices.Count;
	Vector3[] vs = meshToMerge.vertices;

	for(int i=0;i<vs.Length;i++) {
		vs[i] = transformMatrix.MultiplyPoint3x4(vs[i]);
	}
	vertices.AddRange(vs);

	Quaternion rotation = Quaternion.LookRotation(transformMatrix.GetColumn(2), transformMatrix.GetColumn(1));
	Vector3[] ns = meshToMerge.normals;
	if(ns!=null && ns.Length>0) {
		for(int i=0;i<ns.Length;i++) ns[i] = rotation * ns[i];
		normals.AddRange(ns);
	}

	Vector2[] uvs = meshToMerge.uv;
	if(uvs!=null && uvs.Length>0) uv1s.AddRange(uvs);
	uvs = meshToMerge.uv2;
	if(uvs!=null && uvs.Length>0) uv2s.AddRange(uvs);

	for(int i=0;i<ms.Length;i++) {
		if(i<meshToMerge.subMeshCount) {
			int[] ts = meshToMerge.GetTriangles(i);
			if(ts.Length>0) {
				if(ms[i]!=null && !subMeshes.ContainsKey(ms[i])) {
					subMeshes.Add(ms[i], new List<int>());
				}
				List<int> subMesh = subMeshes[ms[i]];
				for(int t=0;t<ts.Length;t++) {
					ts[t] += vertexOffset;
				}
				subMesh.AddRange(ts);
			}
		}
	}
}*/
}



public static class MeshExtension
{
	public static void WeldVertecies (this Mesh mesh, float threshold, float bucketStep) {
         Vector3[] oldVertices = mesh.vertices;
         Vector3[] newVertices = new Vector3[oldVertices.Length];
         int[] old2new = new int[oldVertices.Length];
         int newSize = 0;
     
         // Find AABB
         Vector3 min = new Vector3 (float.MaxValue, float.MaxValue, float.MaxValue);
         Vector3 max = new Vector3 (float.MinValue, float.MinValue, float.MinValue);
         for (int i = 0; i < oldVertices.Length; i++) {
           if (oldVertices[i].x < min.x) min.x = oldVertices[i].x;
           if (oldVertices[i].y < min.y) min.y = oldVertices[i].y;
           if (oldVertices[i].z < min.z) min.z = oldVertices[i].z;
           if (oldVertices[i].x > max.x) max.x = oldVertices[i].x;
           if (oldVertices[i].y > max.y) max.y = oldVertices[i].y;
           if (oldVertices[i].z > max.z) max.z = oldVertices[i].z;
         }
     
         // Make cubic buckets, each with dimensions "bucketStep"
         int bucketSizeX = Mathf.FloorToInt ((max.x - min.x) / bucketStep) + 1;
         int bucketSizeY = Mathf.FloorToInt ((max.y - min.y) / bucketStep) + 1;
         int bucketSizeZ = Mathf.FloorToInt ((max.z - min.z) / bucketStep) + 1;
         List<int>[,,] buckets = new List<int>[bucketSizeX, bucketSizeY, bucketSizeZ];
     
         // Make new vertices
         for (int i = 0; i < oldVertices.Length; i++) {
           // Determine which bucket it belongs to
           int x = Mathf.FloorToInt ((oldVertices[i].x - min.x) / bucketStep);
           int y = Mathf.FloorToInt ((oldVertices[i].y - min.y) / bucketStep);
           int z = Mathf.FloorToInt ((oldVertices[i].z - min.z) / bucketStep);
     
           // Check to see if it's already been added
           if (buckets[x, y, z] == null)
             buckets[x, y, z] = new List<int> (); // Make buckets lazily
     
           for (int j = 0; j < buckets[x, y, z].Count; j++) {
             Vector3 to = newVertices[buckets[x, y, z][j]] - oldVertices[i];
             if (Vector3.SqrMagnitude (to) < threshold) {
               old2new[i] = buckets[x, y, z][j];
               goto skip; // Skip to next old vertex if this one is already there
             }
           }
     
           // Add new vertex
           newVertices[newSize] = oldVertices[i];
           buckets[x, y, z].Add (newSize);
           old2new[i] = newSize;
           newSize++;
     
         skip:;
         }
     
         // Make new triangles
         int[] oldTris = mesh.triangles;
         int[] newTris = new int[oldTris.Length];
         for (int i = 0; i < oldTris.Length; i++) {
           newTris[i] = old2new[oldTris[i]];
         }
         
         Vector3[] finalVertices = new Vector3[newSize];
         for (int i = 0; i < newSize; i++)
           finalVertices[i] = newVertices[i];
     
         mesh.Clear();
         mesh.vertices = finalVertices;
         mesh.triangles = newTris;
         mesh.RecalculateNormals ();
        // mesh.Optimize ();
       }
}
