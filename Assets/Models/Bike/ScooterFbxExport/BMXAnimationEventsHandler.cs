﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMXAnimationEventsHandler : MonoBehaviour 
{

	public void Fix(string sender)
	{
		if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.Fix();
	}

	public void Loop(string sender)
	{
		if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.Loop(sender);
	}
	
	public void Started(string sender)
	{
		if(BMXMainController.Instance)BMXMainController.Instance.InTrick = true;
	//	if(ScoreController.Instance)ScoreController.Instance.AddScoreEvent.Invoke(sender);
		// if(ScoreController.Instance)ScoreController.Instance.AddScoreEvent.Invoke(sender);
		// if(transform.parent.GetComponent<AnimationSystemBikeMenu>())transform.parent.GetComponent<AnimationSystemBikeMenu>().StartedTrick(sender);
	}
	public void _Ended(string sender)
	{
		if (BMXMainController.Instance)
		{
			BMXMainController.Instance.InTrick = false;
			BMXAnimationSystem.Instance.trick = false;
			BMXAnimationSystem.Instance.TrickEnded();
		}
		// if(BMXMainController.Instance)BMXMainController.Instance.EndTrick();
	
		// if(ViewController.Instance)ViewController.Instance.SwitchTricks(0);
		// if(transform.parent.GetComponent<AnimationSystemBikeMenu>())transform.parent.GetComponent<AnimationSystemBikeMenu>().EndedTrick(sender);
	}

	public void _PushGround()
	{
		if(BMXAnimationSystem.Instance){BMXAnimationSystem.Instance.PushOnGround = true;BMXAnimationSystem.Instance.PushOnGroundVoid();}
		//Debug.Log("PushGround");
	}
	
	
	public void PushAir() //Delete in future ScooterRiderNoHatRigFinal->NeutralIdlePosition->Event 0:50
    {
		//if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.PushOnGround = false;
		//Debug.Log("PushAir");
	}
}
