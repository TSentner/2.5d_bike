﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager instance = null;
    public static LevelManager Instance {
        get {
            if (instance == null)
            {
                instance = FindObjectOfType<LevelManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    instance = obj.AddComponent<LevelManager>();
                }
            }
            return instance;
        }
    }

    public Transform startPoint;
    public GameObject bike;
    public UIStatManager uiStatManager;
    //[HideInInspector]
    //public Cinemachine.CinemachineBrain cinemaBrain;


    void Awake()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        if (instance == null)
        {
            //If I am the first instance, make me the Singleton
            instance = this as LevelManager;
            initialization();
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != instance)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void initialization()
    {
        //cinemaBrain = Camera.main.transform.GetComponent<Cinemachine.CinemachineBrain>();
    }

    public void ResetPosition()
    {
        Instance.bike.GetComponent<BikeBaseInput>()?.ResetPosition();
    }
}
