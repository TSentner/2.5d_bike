﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                {
                    GameObject obj = new GameObject();
                    instance = obj.AddComponent<GameManager>();
                }
            }
            return instance;
        }
    }

    public Text logUI;

    void Awake()
    {
        if (!Application.isPlaying)
        {
            return;
        }

        if (instance == null)
        {
            //If I am the first instance, make me the Singleton
            instance = this as GameManager;
            DontDestroyOnLoad(transform.gameObject);
            initialization();
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != instance)
            {
                Destroy(this.gameObject);
            }
        }
    }

    private void initialization()
    {

    }
    
}
