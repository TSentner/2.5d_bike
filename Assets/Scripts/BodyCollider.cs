﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Check if bike on the ground
/// </summary>
public class BodyCollider : MonoBehaviour
{
    public bool IsGrounded { get => CollidedObjects > 0; }

    private int collidedObjects = 0;

    public int CollidedObjects
    {
        get => collidedObjects;
        set {
            if (value >= 0)
                collidedObjects = value;
            else
                collidedObjects = 0;
        }
    }

    public void ResetCountCollidedObjects()
    {
        CollidedObjects = 0;
    }

    private void Update()
    {
        //Debug.Log(CollidedObjects);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
            CollidedObjects++;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
            CollidedObjects--;
    }
}
