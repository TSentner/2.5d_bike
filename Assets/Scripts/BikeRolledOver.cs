﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bike))]
public class BikeRolledOver : MonoBehaviour
{
    private Bike bikeScr;

    private BikeStateMachine<BikeStates.MovementStates> _movement;
    private BikeStateMachine<BikeStates.BikeConditions> _condition;

    private float lastTimeRolledOver = 0f;

    public bool IsRolledOver { get; set; }

    public float timeBeforeRecovery = 2;


    void Start()
    {
        bikeScr = GetComponent<Bike>();
        _movement = bikeScr.MovementState;
        _condition = bikeScr.ConditionState;
    }

    void Update()
    {
        HandleRolledOver();
    }

    private void HandleRolledOver()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal)
            return;

        // if bike has been turned over before
        if (_movement.CurrentState == BikeStates.MovementStates.RolledOver)
        {
            // if we're Rolled Over but one of wheel is grounded
            if (bikeScr.IsWheelGrounded)
            {
                if (bikeScr.CheckIsHorizontalVelocityZero())
                    _movement.ChangeState(BikeStates.MovementStates.Idle);
                else
                    _movement.ChangeState(BikeStates.MovementStates.Movement);
            }
            else if (!bikeScr.IsGrounded) //completely stopped touching the ground
                _movement.ChangeState(BikeStates.MovementStates.Falling);
            else //continue counting in state RolledOver
                RolledOverTime();
        }
        else //it hasn’t been turned over before
        {
            //if bike is on the ground but not on wheels 
            if (bikeScr.IsGrounded && !bikeScr.IsWheelGrounded)
            {
                lastTimeRolledOver = Time.time;
                _movement.ChangeState(BikeStates.MovementStates.RolledOver);
            }
        }
    }

    private void RolledOverTime()
    {
        if (Time.time - lastTimeRolledOver > timeBeforeRecovery)
        {
            _condition.ChangeState(BikeStates.BikeConditions.Recovery);
        }
    }
}
