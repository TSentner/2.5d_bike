﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Show collider when object in air and hide if collision or bike on the ground
/// helps not to fly through the collider
/// </summary>
public class ResetThicknessCollider : MonoBehaviour
{
    public Collider ThicknessCollider;

    private float timeToShow = 0.2f;
    private float? timeLeft = null;

    private void OnCollisionEnter(Collision collision)
    {
        ThicknessCollider.enabled = false;
    }

    private void Update()
    {
        CheckTimeLeft();
    }

    private void CheckTimeLeft()
    {
        if (timeLeft == null)
            return;

        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
        }
        else
        {
            ShowCollider();
        }
    }

    public void StartTimerShowCollider()
    {
        if (timeLeft != null)
            return;

        timeLeft = timeToShow;
    }

    public void HideCollider()
    {
        timeLeft = null;
        ThicknessCollider.enabled = false;
    }

    public void ShowCollider()
    {
        timeLeft = null;
        ThicknessCollider.enabled = true;
    }
}
