using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// states of Bike
/// </summary>
public class BikeStates
{
	/// The possible character conditions
	public enum BikeConditions
	{
		Normal,
        Recovery,
        Pause
    }

    //The possible Movement States the character can be in.
    public enum MovementStates
    {
        Null,
        Idle,
        Movement,
        Falling,
        Jumping,
        Spin,
        RolledOver
    }
}