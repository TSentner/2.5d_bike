﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Bike : MonoBehaviour {

    private UIStatManager uiStatManager;

    // State Machines
    public BikeStateMachine<BikeStates.MovementStates> MovementState;
    public BikeStateMachine<BikeStates.BikeConditions> ConditionState;

    private BaseBikeAbility[] bikeAbilities;

    public Transform ScooterRiderModels;

    [Header("Animation")]
    public BikeAnimationSystem _animationSystem;
    public BikeAnimationEventsHandler _animationHandler;

    [Header("Physics")]
    public Rigidbody rbBike;
    public Transform CenterOfMass;
    private BodyCollider bodyCollider;

    public bool WasGroundedLastFrame { get; set; }
    public bool IsGrounded { get; set; }
    public bool IsWheelGrounded { get; set; }
    public bool IsBothWheelsGrounded { get; private set; }
    public bool JustGotGrounded { get; set; }
    public bool JustOffGrounded { get; set; }
    /// <summary>
    /// 1 we go normally
    /// -1 we go backwards 
    /// </summary>
    private int backToFront = 1;
    public int  BackToFront { get => backToFront; set => backToFront = (int)Mathf.Sign(value); }

    #region UIVariables Properties used only for UI stats
    
    public UIStatManager.UIFloatStatData massStatData;
    [HideInInspector]
    public UnityEvent<float> OnMassBodyChange;
    public float MassBody
    {
        get => rbBike.mass;
        set => Helper.Setter(val => rbBike.mass = val, rbBike.mass, value, OnMassBodyChange, "MassBody");
    }

    public UIStatManager.UIBoolStatData isGravityStatData;
    [HideInInspector]
    public UnityEvent<bool> OnIsGravityChange;
    public bool IsGravity
    {
        get => rbBike.useGravity;
        set => Helper.Setter(val => rbBike.useGravity = val, rbBike.useGravity, value, OnIsGravityChange, "IsGravity");
    }

    public UIStatManager.UIFloatStatData gravityStatData;
    [HideInInspector]
    public UnityEvent<float> OnGravityChange;
    public float Gravity
    {
        get => Physics.gravity.y;
        set => Helper.Setter(val => Physics.gravity = new Vector3(Physics.gravity.x, val, Physics.gravity.z),
                                                    Physics.gravity.y, value, OnGravityChange, "Gravity");
    }

    #endregion


    #region WheelProperties

    private class WheelData
    {
        public Transform meshTransform;
        public WheelCollider col;
        public Vector3 startMeshPos;
        public float meshRotation = 0;
        public float rpmLast = 0;
        public bool isGrounded = false;
        public bool anyGroundedLastFrame = false;
        public RaycastHit hit;
    }

    [HideInInspector]
    private WheelData[] wheels;

    [Header("Wheels")]
    public Transform WBottomBack;
    public Transform WBottomForward;

    public Transform WMeshForward;
    public Transform WMeshBack;

    public WheelCollider WColForward;
    public WheelCollider WColBack;

    #endregion

    

    void Awake () {

        bikeAbilities = GetComponents<BaseBikeAbility>();

        // we initialize our state machines
        MovementState = new BikeStateMachine<BikeStates.MovementStates>();
        MovementState.ChangeState(BikeStates.MovementStates.Idle);
        ConditionState = new BikeStateMachine<BikeStates.BikeConditions>();
        ConditionState.ChangeState(BikeStates.BikeConditions.Normal);

        rbBike = transform.GetComponent<Rigidbody>();
        uiStatManager = LevelManager.Instance.uiStatManager;
        WColForward.steerAngle = 0; // wheels max possible turn 0
        WColBack.steerAngle = 0; 

        //SetupWheels
        wheels = new WheelData[] {
            new WheelData { meshTransform = WMeshBack, col = WColBack, startMeshPos = WMeshBack.transform.localPosition },
            new WheelData { meshTransform = WMeshForward, col = WColForward, startMeshPos = WMeshForward.transform.localPosition }
        };
        foreach (WheelData wheel in wheels)
            wheel.meshTransform.localPosition = new Vector3(wheel.meshTransform.position.x, wheel.meshTransform.position.y, wheel.col.center.z);

        bodyCollider = transform.GetComponent<BodyCollider>();

        SetStats();

        SubscribeBikeCondition();
        SubscribeBikeMovement();
    }

    private void SubscribeBikeMovement()
    {

    }

    private void SubscribeBikeCondition()
    {
        ConditionState.AddListener(BikeStates.BikeConditions.Recovery, StartBikeRecovery);
    }
    

    public void StartBikeRecovery()
    {
        backToFront = 1;
        rbBike.transform.rotation = Quaternion.identity;
        rbBike.velocity = Vector3.zero;
        rbBike.angularVelocity = Vector3.zero;
        MovementState.ChangeState(BikeStates.MovementStates.Idle);
        ConditionState.ChangeState(BikeStates.BikeConditions.Normal);
    }

    private void Start()
    {
        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Player"));

        if (uiStatManager)
        {
            uiStatManager.DrowUIObject(isGravityStatData);
            uiStatManager.DrowUIObject(gravityStatData);
            uiStatManager.DrowUIObject(massStatData, true);
            SubscribeAllUIOject();
        }

        LoadPlayerPrefs();
    }

    private void LoadPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("MassBody"))
            MassBody = PlayerPrefs.GetFloat("MassBody");

        if (PlayerPrefs.HasKey("IsGravity"))
            IsGravity = PlayerPrefs.GetInt("IsGravity") == 1 ? true : false;

        if (PlayerPrefs.HasKey("Gravity"))
            Gravity = PlayerPrefs.GetFloat("Gravity");
    }

    private void SubscribeAllUIOject()
    {
        OnMassBodyChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMassBodyChange, massStatData, MassBody, value => MassBody = value);
        OnIsGravityChange = new UIStatManager.MyBoolEvent();
        uiStatManager.SubscribeUIOject(OnIsGravityChange, isGravityStatData, IsGravity, value => IsGravity = value);
        OnGravityChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnGravityChange, gravityStatData, Gravity, value => Gravity = value);
    }

    private void FixedUpdate()
    {
        SetStats();
        UpdateColliders();
    }

    private void UpdateColliders()
    {
        if (IsBothWheelsGrounded)
        {
            gameObject.GetComponent<ResetThicknessCollider>()?.HideCollider();
            //bodyCollider.ResetCountCollidedObjects();
        }
        else
            gameObject.GetComponent<ResetThicknessCollider>()?.StartTimerShowCollider();
    }

    void Update()
    {
        //always must be first
        ResetWColTorque();
        UpdateWheelsData();
        CheckWColIsGroundedError();

        CheckIsGround();
        HandleCharacterStatus();

        //always must be last
        SetWheelsLastFrameData();

        UpdateAbilities();

        UpdateAnimators();
        // GameManager.Instance.logUI.text = MovementState.CurrentState.ToString();
    }

    private void UpdateAbilities()
    {
        foreach (BaseBikeAbility ability in bikeAbilities)
        {
            if (ability.enabled && ability.AbilityInitialized)
            {
                ability.UpdateAbilities();
            }
        }
    }

    private void UpdateAnimators()
    {
        foreach (BaseBikeAbility ability in bikeAbilities)
        {
            if (ability.enabled && ability.AbilityInitialized)
            {
                ability.UpdateAnimator();
            }
        }
    }

    /// <summary>
    /// For stickiness at the wheelCollider the spring is small and the Damper is large. 
    /// Therefore, the wheel at rest may be inadequate, as if to fall asleep.
    /// Maybe on earth but isGrounded = false;
    /// Solution: momentarily set torque
    /// </summary>
    private void CheckWColIsGroundedError()
    {
        int i = 0;
        foreach (WheelData wheel in wheels)
        {
            if (!wheel.col.isGrounded && wheel.anyGroundedLastFrame
                || wheel.col.isGrounded && !wheel.anyGroundedLastFrame)
            {
                SetSmallTorqueWCol();
            }
            i++;
        }
    }

    /// <summary>
    /// Reset Torque every frame
    /// We do not use Torque except wakeup functions, so its reset should be in one place.
    /// </summary>
    private void ResetWColTorque()
    {
        foreach (WheelData wheel in wheels)
            wheel.col.motorTorque = 0;
    }

    /// <summary>
    /// Usually need to wake up wheelColliders
    /// </summary>
    /// <param name="i">wheel Index; if i less 0 then for all wheels</param>
    /// <param name="isForAll">, Index not counted</param>
    public void SetSmallTorqueWCol(float direction = 0)
    {
        if (direction == 0)
            direction = Mathf.Sign(rbBike.velocity.z);

        float torque = 5f * direction * Time.deltaTime;
        foreach (WheelData wheel in wheels)
            wheel.col.motorTorque = torque;
    }

    /// <summary>
    /// Position of the wheel mesh according to WheelCollider
    /// </summary>
    private void UpdateWheelsData()
    {
        foreach (WheelData wheel in wheels)
        {
            RaycastHit hit;
            //WheelHit hit;
            //if (wheel.col.GetGroundHit(out hit)) // Is late
            var wheelCenter = wheel.col.transform.TransformPoint(wheel.col.center);
            float distance = (wheel.col.transform.position - wheel.col.transform.TransformPoint(Vector3.up * (wheel.col.radius + wheel.col.suspensionDistance))).magnitude;
            //Debug.DrawLine(wheelCenter, wheelCenter - wheel.col.transform.up * distance);
            if (Physics.Raycast(wheelCenter, -wheel.col.transform.up, out hit, distance, LayerMask.GetMask("Ground"), QueryTriggerInteraction.Ignore))
            {
                wheel.isGrounded = true;

                // Update WheelMesh
                wheel.meshTransform.position = hit.point + wheel.col.transform.up * wheel.col.radius * wheel.col.transform.lossyScale.y;
                //wheel.meshTransform.position = hit.point + (wheel.col.transform.up * (wheel.meshTransform.lossyScale.z / 2));
            }
            else
            {
                wheel.isGrounded = false;

                // Update WheelMesh
                //wheel.meshTransform.localPosition = wheel.col.center - Vector3.up * (wheel.col.radius - (wheel.col.radius - wheel.col.suspensionDistance));
            }

            if (MovementState.CurrentState == BikeStates.MovementStates.Idle) //Idle - don't rotate Mesh
                wheel.rpmLast = 0;
            else if (wheel.col.isGrounded) //remember last rpm because the wheel doesn’t spin above the ground 
                wheel.rpmLast = wheel.col.rpm;

            //Repeat - keeps an angle between 0 and 360 degrees
            wheel.meshRotation = Mathf.Repeat(wheel.meshRotation + Time.fixedDeltaTime * wheel.rpmLast * 360.0f / 60.0f, 360.0f); //determine the current turn wheelCollider
            wheel.meshTransform.localRotation = Quaternion.Euler(wheel.meshRotation, wheel.col.steerAngle, 90.0f);
        }

        RotateScooterRiderModels();
    }

    private void RotateScooterRiderModels()
    {
        Vector3 forwardBP = WMeshForward.position - transform.up * WMeshForward.lossyScale.x / 2; // Forward Bottom Point
        Vector3 backwardBP = WMeshBack.position - transform.up * WMeshBack.lossyScale.x / 2; // Backward Bottom Point

        if (IsBothWheelsGrounded) //if forward & rear wheel grounded
        {
            //align the front wheel ScooterRiderModels
            Vector3 diff = forwardBP - WBottomForward.position;
            ScooterRiderModels.position = ScooterRiderModels.position + new Vector3(0, diff.y, diff.z);

            //align the rear wheel
            Vector3 ForwardToBack = backwardBP - forwardBP; //Vector From forwardBP To backwardBP
            ForwardToBack = new Vector3(0, ForwardToBack.y, ForwardToBack.z);
            Vector3 ForwardToScooterBack = WBottomBack.position - forwardBP; //Vector From forwardBP To scooter current backward point
            ForwardToScooterBack = new Vector3(0, ForwardToScooterBack.y, ForwardToScooterBack.z);
            float angleDiff = Vector3.Angle(ForwardToBack, ForwardToScooterBack);
            angleDiff *= Mathf.Sign(Vector3.Cross(ForwardToBack, ForwardToScooterBack).x);
            ScooterRiderModels.RotateAround(forwardBP, Vector3.right, -angleDiff);

            //Debug.Log(angleDiff); 
            //Debug.DrawLine(forwardBP, forwardBP + ForwardToBack, Color.blue);
            //Debug.DrawLine(forwardBP, forwardBP + ForwardToScooterBack, Color.red);
        }
        else if (wheels[1].isGrounded) //if only forward wheel grounded
        {
            //align the front wheel ScooterRiderModels
            Vector3 diff = forwardBP - WBottomForward.position;
            ScooterRiderModels.position = ScooterRiderModels.position + new Vector3(0, diff.y, diff.z);
        }
        else if (wheels[0].isGrounded) //if only rear wheel grounded
        {
            //align the back wheel ScooterRiderModels
            Vector3 diff = ScooterRiderModels.InverseTransformDirection(backwardBP - WBottomBack.position);
            ScooterRiderModels.localPosition = ScooterRiderModels.localPosition + Vector3.up * diff.y;
        }

    }

    /// <summary>
    /// Handles the character status.
    /// </summary>
    protected virtual void HandleCharacterStatus()
    {
        /*if (characterCondition.CurrentState == BikeConditions.TypeConditions.Recovery)
        {
            return;
        }
        */
    }

    public bool CheckIsHorizontalVelocityZero()
    {
        //Debug.Log(rbBike.velocity.ToString("F9"));
        //Debug.Log(rbBike.angularVelocity.ToString("F9"));
        //rbBike.angularVelocity.Approximately(Vector3.zero, 0.01f);

        return rbBike.velocity.z.Approximately(.0f, 0.002f);

    }

    private void CheckIsGround()
    {
        WasGroundedLastFrame = IsGrounded;

        IsWheelGrounded = wheels[0].isGrounded || wheels[1].isGrounded;
        
        IsBothWheelsGrounded = wheels[0].isGrounded && wheels[1].isGrounded;

        IsGrounded = IsWheelGrounded;
        if (bodyCollider)
            IsGrounded |= bodyCollider.IsGrounded;

        JustGotGrounded = !WasGroundedLastFrame && IsGrounded;
        JustOffGrounded = WasGroundedLastFrame && !IsGrounded;
        
    }

    /// <summary>
    /// In case you change the data in the editor (Events will not work).
    /// </summary>
    private void SetStats()
    {
        rbBike.centerOfMass = CenterOfMass.localPosition;
        //CenterOfMass.localPosition = rbBike.centerOfMass;
    }

    /// <summary>
    /// remember the last values for wheelColliders
    /// </summary>
    private void SetWheelsLastFrameData()
    {
        foreach (WheelData wheel in wheels)
        {
            wheel.anyGroundedLastFrame = wheel.col.isGrounded;
        }
    }
}
