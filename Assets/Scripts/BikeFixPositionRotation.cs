﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bike))]
public class BikeFixPositionRotation : MonoBehaviour
{
    private Bike bikeScr;

    private RigidbodyConstraints bufConstraints;

    public Rigidbody fixRB;

    public bool isFixXPosition = true;
    public bool isFixYZRotation = true;
    public bool isFixAngularVelocity = true;
    public bool isFixLocalRotation = true;

    public float positionX;

    void Start()
    {
        bikeScr = GetComponent<Bike>();

        if (!fixRB)
            fixRB = GetComponent<Rigidbody>();

        positionX = fixRB.position.x;
    }

    private void FixedUpdate()
    {
        FixXPosition();
        FixYZRotation();
    }

    /// <summary>
    /// Fix a physical object in the same plane for position
    /// </summary>
    private void FixXPosition()
    {
        if (isFixXPosition)
        {
            fixRB.transform.position = new Vector3(positionX, fixRB.transform.position.y, fixRB.transform.position.z);
            //now also for physics
            fixRB.velocity = new Vector3(0, fixRB.velocity.y, fixRB.velocity.z);
        }
    }

    /// <summary>
    /// Fix a physical object in the same plane for rotation
    /// </summary>
    private void FixYZRotation()
    {
        if (!isFixYZRotation)
            return;

        if (isFixLocalRotation)
        {
            Vector3 targetUp;
            Vector3 targetForward;
            GetYZPlaneForwardUp(out targetForward, out targetUp, fixRB.transform.localRotation);
            targetForward *= bikeScr.BackToFront;

            fixRB.transform.localRotation = Quaternion.LookRotation(targetForward, targetUp);
        }

        if (isFixAngularVelocity) //now also for physics
            fixRB.angularVelocity = Vector3.right * fixRB.angularVelocity.x;
    }

    public void Freeze(bool isFreeze)
    {
        isFixLocalRotation = isFreeze;
        if (isFreeze)
        {
            fixRB.constraints = bufConstraints;
        }
        else
        {
            bufConstraints = fixRB.constraints;
            fixRB.constraints = RigidbodyConstraints.None | RigidbodyConstraints.FreezePositionX;
            //fixRB.constraints = RigidbodyConstraints.None;
        }
    }

    public void GetYZPlaneForwardUp(out Vector3 targetForward, out Vector3 targetUp, Quaternion rotation)
    {
        targetUp = rotation * Vector3.up; //get up vector
        targetForward = new Vector3(0, -targetUp.z, targetUp.y);//rotate vector by 90 without x axis
        if (targetForward.magnitude == 0)
        {
            targetForward = Mathf.Sign(targetUp.x) * Vector3.forward;
        }
        targetForward.Normalize();
        targetUp = new Vector3(0, targetForward.z, -targetForward.y);// get up from forward without x axis
    }
}
