﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIStatManager : MonoBehaviour
{
    public delegate void OnValueChangeDelegate(float newVal);
    public class MyFloatEvent : UnityEvent<float>
    {
    }
    public class MyBoolEvent : UnityEvent<bool>
    {
    }

    public abstract class UIStatData
    {
        public Transform UITransform;
        public string varName;
        public string txtName;
    }

    [System.Serializable]
    public class UIFloatStatData : UIStatData
    {
        public float min;
        public float max;
    }

    [System.Serializable]
    public class UIBoolStatData : UIStatData
    {
        
    }

    public GameObject BikeGO;
    public Transform UIRoot;
    public GameObject UISliderPrefab;
    public GameObject UICheckBoxPrefab;

    private Bike bikeScr;
    private float nextBlockOffset = 0;

    void Start()
    {
        bikeScr = LevelManager.Instance.bike.GetComponent<Bike>();
    }

    public void ResizeUIRoot()
    {
        if (!UIRoot)
            return;

        RectTransform rt = UIRoot.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, nextBlockOffset + 15f); //Width, Height
    }

    public void DrowUIObject(UIStatData statData, bool isNeedResize = false)
    {
        if (statData.varName == "")
        {
            if (isNeedResize)
                ResizeUIRoot();
            return;
        }
            

        UIFloatStatData statSliderData = statData as UIFloatStatData;
        UIBoolStatData statCheckBoxData = statData as UIBoolStatData;
        
        if (!statData.UITransform) //Create UI block if he is not create
        {
            GameObject UIPrefab = null;

            if (statSliderData != null)
                UIPrefab = UISliderPrefab;

            if (statCheckBoxData != null)
                UIPrefab = UICheckBoxPrefab;

            statData.UITransform = ((GameObject)Instantiate(UIPrefab, Vector3.zero, Quaternion.identity)).transform;
            statData.UITransform.SetParent(UIRoot, true);
            statData.UITransform.gameObject.name = statData.varName;
            Transform txtNameT = statData.UITransform.Find("TxtName");
            if (txtNameT)
                txtNameT.GetComponent<Text>().text = statData.txtName;

            RectTransform rt = statData.UITransform.GetComponent<RectTransform>();
            rt.localPosition = Vector2.zero;
            rt.localScale = Vector3.one;
            rt.anchoredPosition = new Vector2(0, -(rt.sizeDelta.y / 2 + nextBlockOffset + UIPrefab.GetComponent<RectTransform>().localPosition.y));
            rt.offsetMin = new Vector2(UIPrefab.GetComponent<RectTransform>().offsetMin.x, rt.offsetMin.y); //Left, Bottom
            rt.offsetMax = new Vector2(UIPrefab.GetComponent<RectTransform>().offsetMax.x, rt.offsetMax.y); ; //Right, Top

            nextBlockOffset += rt.sizeDelta.y;
        }

        if (statSliderData != null)
        {
            Transform sliderT = statData.UITransform.Find("Slider");
            if (sliderT)
            {
                sliderT.GetComponent<Slider>().minValue = statSliderData.min;
                sliderT.GetComponent<Slider>().maxValue = statSliderData.max;
            }
        }
        

        if (isNeedResize)
            ResizeUIRoot();
    }

    #region statDataFunctionTemplate Templates for installing event of property and slider

    //Template: will form a function for UnityEvent and reduce the code
    public UnityAction<float> CreatorRefreshUIAction (UIFloatStatData statData, float defaultVal)
    {
        if (statData.UITransform.Find("StatValue"))
            statData.UITransform.Find("StatValue").GetComponent<Text>().text = defaultVal.ToString();

        if (statData.UITransform.Find("Slider"))
            statData.UITransform.Find("Slider").GetComponent<Slider>().value = defaultVal;
        UnityAction<float> refreshUI = (float val) =>
           {
               if (statData.UITransform.Find("StatValue"))
                   statData.UITransform.Find("StatValue").GetComponent<Text>().text = val.ToString();

               if (statData.UITransform.Find("Slider"))
                   statData.UITransform.Find("Slider").GetComponent<Slider>().value = val;
           };
        return refreshUI;
    }

    public UnityAction<bool> CreatorRefreshUIAction(UIBoolStatData statData, bool defaultVal)
    {
        statData.UITransform.GetComponent<Toggle>().isOn = defaultVal;
        UnityAction<bool> refreshUI = (bool val) =>
        {
            statData.UITransform.GetComponent<Toggle>().isOn = val;
        };
        return refreshUI;
    }

    public void AddSliderListener(UIFloatStatData statData, Action<float> setVal)
    {
        Slider slider = statData.UITransform?.Find("Slider")?.GetComponent<Slider>();
        if (slider)
            slider.onValueChanged.AddListener(delegate {
                setVal(slider.value);
            });
    }
    public void AddCheckBoxListener(UIBoolStatData statData, Action<bool> setVal)
    {
        Toggle toggle = statData.UITransform?.GetComponent<Toggle>();
        if (toggle)
            toggle.onValueChanged.AddListener(delegate {
                setVal(toggle.isOn);
            });
    }


    #endregion

    public void SubscribeUIOject(UnityEvent<float> uEvent, UIFloatStatData statData, float ValProperty, Action<float> setVal)
    {
        if (statData.varName == "")
            return;

        uEvent.AddListener(CreatorRefreshUIAction(statData, ValProperty));
        AddSliderListener(statData, setVal);
    }


    public void SubscribeUIOject(UnityEvent<bool> uEvent, UIBoolStatData statData, bool ValProperty, Action<bool> setVal)
    {
        if (statData.varName == "")
            return;

        uEvent.AddListener(CreatorRefreshUIAction(statData, ValProperty));
        AddCheckBoxListener(statData, setVal);
    }
}
