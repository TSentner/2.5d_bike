﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleEnable : MonoBehaviour
{
    public bool toggleAtStart = false;

    [System.Serializable]
    public class Switchable
    {
        [HideInInspector]
        public float bufAlpha;
        public bool canvasGroupAlpha = false;
        public GameObject gameObject;
    }

    public List<Switchable> switchableList;

    private void Start()
    {
        foreach (var switchable in switchableList)
            if (switchable.canvasGroupAlpha)
                switchable.bufAlpha = switchable.gameObject.GetComponent<CanvasGroup>().alpha;

        if (toggleAtStart)
            toggle();
    }

    public void toggle()
    {
        foreach (var switchable in switchableList)
            if (switchable.canvasGroupAlpha)
            {
                var cg = switchable.gameObject.GetComponent<CanvasGroup>();
                if (Mathf.Approximately(cg.alpha, 0.000001f))
                    cg.alpha = switchable.bufAlpha;
                else
                    cg.alpha = 0.000001f;
            }
            else
                switchable.gameObject.SetActive(!switchable.gameObject.activeSelf);
    }
}
