﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BikeAnimationEventsHandler : MonoBehaviour
{
    [HideInInspector]
    public UnityEvent OnStartPush;
    [HideInInspector]
    public UnityEvent OnEndPush;
    [HideInInspector]
    public UnityEvent OnEndReturnHip;

    #region OldEvents Delete in future
    public void _Fix(string sender) //Delete in future
    {
		//if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.Fix();
	}

	public void _Loop(string sender) //Delete in future
    {
		//if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.Loop(sender);
	}
	
	public void _Started(string sender) //Delete in future
    {
		/*if(BMXMainController.Instance)BMXMainController.Instance.InTrick = true;
	*/
        //	if(ScoreController.Instance)ScoreController.Instance.AddScoreEvent.Invoke(sender);
		    // if(ScoreController.Instance)ScoreController.Instance.AddScoreEvent.Invoke(sender);
		    // if(transform.parent.GetComponent<AnimationSystemBikeMenu>())transform.parent.GetComponent<AnimationSystemBikeMenu>().StartedTrick(sender);
	}
	public void _Ended(string sender) //Delete in future
    {
		/*if (BMXMainController.Instance)
		{
			BMXMainController.Instance.InTrick = false;
			BMXAnimationSystem.Instance.trick = false;
			BMXAnimationSystem.Instance.TrickEnded();
		}
        */
		// if(BMXMainController.Instance)BMXMainController.Instance.EndTrick();
	
		// if(ViewController.Instance)ViewController.Instance.SwitchTricks(0);
		// if(transform.parent.GetComponent<AnimationSystemBikeMenu>())transform.parent.GetComponent<AnimationSystemBikeMenu>().EndedTrick(sender);
	}

	public void _PushGround() //Delete in future
    {
		//if(BMXAnimationSystem.Instance){BMXAnimationSystem.Instance.PushOnGround = true;BMXAnimationSystem.Instance.PushOnGroundVoid();}
		//Debug.Log("PushGround");
	}
	
	
	public void _PushAir() //Delete in future ScooterRiderNoHatRigFinal->NeutralIdlePosition->Event 0:50
    {
		//if(BMXAnimationSystem.Instance)BMXAnimationSystem.Instance.PushOnGround = false;
		//Debug.Log("PushAir");
	}
    #endregion

    void Awake()
    {
        OnStartPush = new UnityEvent();
        OnEndReturnHip = new UnityEvent();
    }

    #region PushAnimation
    public void StartPush()
    {
        OnStartPush?.Invoke();
    }

    public void EndPush()
    {
        OnEndPush?.Invoke();
    }

    public void EndReturnHip()
    {
        OnEndReturnHip?.Invoke();
    }
    #endregion
}
