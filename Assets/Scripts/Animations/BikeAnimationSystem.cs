﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Animator))]
public class BikeAnimationSystem : MonoBehaviour
{
    [HideInInspector]
    public Animator animator;

    public enum IKAnimationType
    {
        tricksAnimations,
        tricksAnimationsScooter,
        movementCondition
    }

    [Serializable]
    public class IKAnimationData
    {
        public string animationName;
        public bool IKHandR;
        public bool IKHandL;
        public bool IKFootR;
        public bool IKFootL;
    }
    public IKAnimationData[] tricksAnimations;
    public IKAnimationData[] tricksAnimationsScooter;
    public IKAnimationData[] movementCondition;

    public HipPositionController HipPosition;

    public InverseKinematics IKHandR;
    public InverseKinematics IKHandL;
    public InverseKinematics IKFootR;
    public InverseKinematics IKFootL;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        SetIKAnimationData(IKAnimationType.movementCondition, "Idle1");
    }

    //[Range(0, 1)]
    //public float time_t;
    void Update()
    {
/*
        var force_push = BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff") ||
                         BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff 0");

        if (force_push)
        {
            BMXAnimator.Play(BMXAnimator.GetCurrentAnimatorStateInfo(0).IsName("Pushoff") ? "Pushoff" : "Pushoff 0", 0, time_t);
        }

        HipPosition.Trick = !bunny_hope && !landing && !lean && !stand && !force_push && !neutral;
  */       
    }

    public void SetIKAnimationData(IKAnimationData IKData)
    {
        IKHandR.Active = IKData.IKHandR;
        IKHandL.Active = IKData.IKHandL;
        IKFootR.Active = IKData.IKFootR;
        IKFootL.Active = IKData.IKFootL;
    }
    public void SetIKAnimationData(IKAnimationType typeIK, string IKName)
    {
        IKAnimationData[] IKData;
        switch (typeIK)
        {
            case IKAnimationType.tricksAnimations:
                IKData = tricksAnimations;
                break;
            case IKAnimationType.tricksAnimationsScooter:
                IKData = tricksAnimationsScooter;
                break;
            case IKAnimationType.movementCondition:
                IKData = movementCondition;
                break;
            default:
                return;
                break;
        }
        
        IKAnimationData rowIK = (from ikData in IKData
                                 where ikData.animationName == IKName
                                 select ikData).LastOrDefault();
        if (rowIK != null)
            SetIKAnimationData(rowIK);


        if (IKName == "Push")
            HipPosition.Smooth = 0.1f;
        else
            HipPosition.Smooth = 0.3f;

        if (IKName == "Idle" || IKName == "Push")
            HipPosition.isStand = true;
        else
            HipPosition.isStand = false;
    }

    internal void ResetIKAnimationData()
    {
        IKHandR.Active = IKHandL.Active = IKFootR.Active = IKFootL.Active = false;
    }
}
