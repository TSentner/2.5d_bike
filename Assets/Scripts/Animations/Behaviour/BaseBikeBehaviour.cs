﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBikeBehaviour : StateMachineBehaviour
{
    [SerializeField]
    protected string ikName;
    [SerializeField]
    protected BikeAnimationSystem.IKAnimationType ikType;

    /// <summary>
    /// Try get AnimationSystemComponent from animator object
    /// </summary>
    protected BikeAnimationSystem GetAnimationSystem(Animator animator)
    {
        return animator.gameObject.GetComponent<BikeAnimationSystem>();
    }

    protected void SetIKAnimationData(Animator animator)
    {
        var _animationSystem = GetAnimationSystem(animator);
        if (!_animationSystem)
            return;

        _animationSystem.SetIKAnimationData(ikType, ikName);

    }
        

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SetIKAnimationData(animator);
    }
    
}
