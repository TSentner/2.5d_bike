﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ConstRotator : MonoBehaviour
{
    private UIStatManager uiStatManager;

    public UIStatManager.UIFloatStatData speedYStatData;
    public Vector3 speed;

    [HideInInspector]
    public UnityEvent<float> OnSpeedYChange;
    public float SpeedY
    {
        get => speed.y;
        set => Helper.Setter(val => speed = new Vector3(speed.x, val, speed.z), speed.y, value, OnSpeedYChange, "SpeedY");
    }

    private void Start()
    {
        uiStatManager = LevelManager.Instance.uiStatManager;
        if (uiStatManager)
        {
            uiStatManager.DrowUIObject(speedYStatData, true);
            SubscribeUIOject();
        }
        LoadPlayerPrefs();
    }

    private void LoadPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("SpeedY"))
            SpeedY = PlayerPrefs.GetFloat("SpeedY");
    }

    private void SubscribeUIOject()
    {
        OnSpeedYChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnSpeedYChange, speedYStatData, SpeedY, value => SpeedY = value);
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation *= Quaternion.Euler(speed * Time.deltaTime);
    }
}
