﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Bike))]
public class BikeBaseInput : MonoBehaviour
{
    private Bike bikeScr;
    public  Bike BikeScr {
        get
        {
            if (bikeScr == null)
            {
                bikeScr = GetComponent<Bike>();
            }
            return bikeScr;
        }
    }
    private BikeStateMachine<BikeStates.MovementStates> _movement;
    private BikeStateMachine<BikeStates.BikeConditions> _condition;

    public Transform resetPosition;

    private void Awake()
    {
        if (resetPosition == null)
            resetPosition = LevelManager.Instance.startPoint;
    }

    // Start is called before the first frame update
    void Start()
    {
        bikeScr = GetComponent<Bike>();
        _movement = BikeScr.MovementState;
        _condition = BikeScr.ConditionState;
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("ResetPosition"))
        {
            ResetPosition();
        }

        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            PlayerPrefs.DeleteAll();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void ResetPosition()
    {
        BikeScr.transform.position = resetPosition.position;
        BikeScr.StartBikeRecovery();
    }
}
