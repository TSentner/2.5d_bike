﻿using System;
using System.Collections;
using System.Collections.Generic;

using JetBrains.Annotations;
using UnityEngine;

public class HipPositionController : MonoBehaviour
{
    public BikeAnimationEventsHandler _animationHandler;

    public bool active = true;

    public bool isBunnyHope;

    public bool isPartialy;

    public bool isTrick;

    public bool isLanding;
    public bool isLean;
    
    public bool isStand;
    public float LeanCoef;

    

    public Transform hip;
    public Transform hipTarget;
    public Transform hipStartPosition;
    public Transform standPosition;
    
    public AnimationCurve PushOffsetCurveY;
    public AnimationCurve PushOffsetCurveX;
    public AnimationCurve PushOffsetCurveZ;
    
    public AnimationCurve BunnyHopeOffset;
    public AnimationCurve LandingOffset;
    
    [Range(.0f, 1f)]
    public float progress; //AnimationCurve progress

    private float Angle;
    public bool Hill;

    public float Smooth = 0.08f;
    private bool prevTrick;


   public  void SetHillParams(bool _hill, float angle)
    {
        Hill = _hill;
        Angle = angle;
        Angle = Mathf.Clamp(Angle, -40, 40);
    }

    private void Start()
    {
        _animationHandler.OnStartPush.AddListener(StartPush);
        hip.position = hipStartPosition.position;

        //hipTargetStart = hipStartPosition.localPosition;
        //yield return new WaitForEndOfFrame();
    }

    private void Update()
    {

    }

    void LateUpdate()
    {
        if (!active)
            return;

        Vector3 hipTargetStart = hipStartPosition.localPosition;

        if (isBunnyHope)
        {
            // hip.position =  Vector3.Lerp(hip.position, hip.position+Vector3.up*BunnyHopeOffset.Evaluate(t),.8f);
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition,
                hipTargetStart + Vector3.up * BunnyHopeOffset.Evaluate(progress), Smooth);
       

        } else if (isLanding)
        {
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition,
                hipTargetStart + Vector3.up * LandingOffset.Evaluate(progress), Smooth);
      
        } 
        else if (isLean)
        {
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition, hipTargetStart +
                                                                            Vector3.right *((LeanCoef* 0.09f)) + Vector3.up*0.1f*-Mathf.Clamp01(-LeanCoef),
                Smooth);
          
        } 
        else if (Hill)
        {
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition, hipTargetStart +
                                                                            transform.InverseTransformDirection(
                                                                                Vector3.right) * 0.005f * (-Angle),
                Smooth);
        
        } else if (isStand)
        {
            progress = CalculatePushProgress();
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition,
                standPosition.localPosition + Vector3.up * PushOffsetCurveY.Evaluate(progress)
                + Vector3.right * PushOffsetCurveX.Evaluate(progress), Smooth);

        }
        else if  (!isTrick)
        {
            hipTarget.localPosition = Vector3.Lerp(hipTarget.localPosition, hipTargetStart, Smooth);
                 
        } else if (isTrick)
        {
            // hipResultPosition = Vector3.Lerp(hipTarget.position, hip.position, Smooth); //!!!!!!!!!!!!!!!!!!!
            hipTarget.position = Vector3.Lerp(hipTarget.position, hip.position, Smooth);
        }

        hip.position = hipTarget.position;
    }

    #region PushAnimation
    private float startPush;
    private float startReturnHip = 0.27f; // time betwen startPush and startReturnHip
    private float endReturnHip = 0.27f; // time betwen startReturnHip and endReturnHip


    private float CalculatePushProgress()
    {
        float currentTime = Time.time - startPush;
        if (currentTime < startReturnHip) //we betwen startPush and startReturnHip
        {
            return currentTime * 0.5f / startReturnHip;
        }
        else if((currentTime - startReturnHip) < endReturnHip) //we betwen startReturnHip and endReturnHip
        {
            return 0.5f + (currentTime - startReturnHip) * 0.5f / endReturnHip;
        }

        return 0;
    }

    public void StartPush()
    {
        startPush = Time.time;
    }

    #endregion
}
