﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using UnityEngine;

//[ExecuteInEditMode]
public class InverseKinematics : MonoBehaviour
{
	public Transform upperArm;
	public Transform forearm;
	public Transform hand;
	public Transform elbow;
	public Transform target;
	public Transform upperArmNoActivePos;
	public Transform forearmNoActivePos;
	public Transform handNoActivePos;

	[Space(20)] public Vector3 uppperArm_OffsetRotation;
	public Vector3 forearm_OffsetRotation;
	public Vector3 hand_OffsetRotation;
	[Space(20)] public bool handMatchesTargetRotation = true;
	[Space(20)] public bool debug;
	float angle;
	float upperArm_Length;
	float forearm_Length;
	float arm_Length;
	float targetDistance;
	float adyacent;
	public bool Active;


	private float timer;
	private float timer2 = 1.5f;
	private Quaternion t_upperArm;
	private Quaternion t_forearm;
	private Quaternion t_hand;
	private Quaternion ta_upperArm;
	private Quaternion ta_forearm;
	private Quaternion ta_hand;
	public float intensity;
	public float intensityCoef;

	public LayerMask GroundMask;


	public bool ActiveByT;
	public bool T_Active;

	// Use this for initialization
	void Start()
	{
		//Debug.Log("ik");
	}


	void FixedUpdate()
	{
	
	}

	private float y;

	void LateUpdate()
	{
		intensity = Mathf.Lerp(intensity, (Active) ? 1 : 0, intensityCoef);
		if (intensity < 0.95f)
		{
			if (upperArm != null && forearm != null && hand != null && elbow != null && target != null &&
			    intensity >= 0.01f)
			{
				upperArm.LookAt(target, elbow.position - upperArm.position);
				upperArm.Rotate(uppperArm_OffsetRotation);
				Vector3 cross = Vector3.Cross(elbow.position - upperArm.position, forearm.position - upperArm.position);
				upperArm_Length = Vector3.Distance(upperArm.position, forearm.position);
				forearm_Length = Vector3.Distance(forearm.position, hand.position);
				arm_Length = upperArm_Length + forearm_Length;
				targetDistance = Vector3.Distance(upperArm.position, target.position);
				targetDistance = Mathf.Min(targetDistance, arm_Length - arm_Length * 0.001f);
				adyacent = ((upperArm_Length * upperArm_Length) - (forearm_Length * forearm_Length) +
				            (targetDistance * targetDistance)) / (2 * targetDistance);
				angle = Mathf.Acos(adyacent / upperArm_Length) * Mathf.Rad2Deg;
				if (!float.IsNaN(upperArm.position.x) && !float.IsNaN(upperArm.position.y) &&
				    !float.IsNaN(upperArm.position.z))
					upperArm.RotateAround(upperArm.position, cross, -angle);
				forearm.LookAt(target, cross);
				forearm.Rotate(forearm_OffsetRotation);
				if (handMatchesTargetRotation)
				{
					hand.rotation = target.rotation;
					hand.Rotate(hand_OffsetRotation);
				}

				if (debug)
				{
					if (forearm != null && elbow != null)
					{
						Debug.DrawLine(forearm.position, elbow.position, Color.blue);
					}

					if (upperArm != null && target != null)
					{
						Debug.DrawLine(upperArm.position, target.position, Color.red);
					}
				}

				upperArmNoActivePos.transform.rotation = Quaternion.Slerp(upperArmNoActivePos.transform.rotation,
					upperArm.transform.rotation, intensity);
				forearmNoActivePos.transform.rotation = Quaternion.Slerp(forearmNoActivePos.transform.rotation,
					forearm.transform.rotation, intensity);
				handNoActivePos.transform.rotation = Quaternion.Slerp(handNoActivePos.transform.rotation,
					hand.transform.rotation, intensity);

				// upperArmNoActivePos.transform.eulerAngles = Vector3.Slerp(upperArmNoActivePos.transform.eulerAngles,upperArm.transform.eulerAngles, intensity);
				// forearmNoActivePos.transform.eulerAngles = Vector3.Slerp(forearmNoActivePos.transform.eulerAngles,forearm.transform.eulerAngles, intensity);
				// handNoActivePos.transform.eulerAngles = Vector3.Slerp(handNoActivePos.transform.eulerAngles,hand.transform.eulerAngles, intensity);
			}
		}
		else
		{
			if (upperArmNoActivePos != null && forearmNoActivePos != null && handNoActivePos != null && elbow != null &&
			    target != null && intensity >= 0.01f)
			{
				upperArmNoActivePos.LookAt(target, elbow.position - upperArmNoActivePos.position);
				upperArmNoActivePos.Rotate(uppperArm_OffsetRotation);
				Vector3 cross = Vector3.Cross(elbow.position - upperArmNoActivePos.position,
					forearmNoActivePos.position - upperArmNoActivePos.position);
				upperArm_Length = Vector3.Distance(upperArmNoActivePos.position, forearmNoActivePos.position);
				forearm_Length = Vector3.Distance(forearmNoActivePos.position, handNoActivePos.position);
				arm_Length = upperArm_Length + forearm_Length;
				targetDistance = Vector3.Distance(upperArmNoActivePos.position, target.position);
				targetDistance = Mathf.Min(targetDistance, arm_Length - arm_Length * 0.001f);
				adyacent = ((upperArm_Length * upperArm_Length) - (forearm_Length * forearm_Length) +
				            (targetDistance * targetDistance)) / (2 * targetDistance);
				angle = Mathf.Acos(adyacent / upperArm_Length) * Mathf.Rad2Deg;
				if (!float.IsNaN(upperArmNoActivePos.position.x) && !float.IsNaN(upperArmNoActivePos.position.y) &&
				    !float.IsNaN(upperArmNoActivePos.position.z))
					upperArmNoActivePos.RotateAround(upperArmNoActivePos.position, cross, -angle);
				forearmNoActivePos.LookAt(target, cross);
				forearmNoActivePos.Rotate(forearm_OffsetRotation);
				if (handMatchesTargetRotation)
				{
					handNoActivePos.rotation = target.rotation;
					handNoActivePos.Rotate(hand_OffsetRotation);
				}

				

				if (debug)
				{
					if (forearmNoActivePos != null && elbow != null)
					{
						Debug.DrawLine(forearmNoActivePos.position, elbow.position, Color.blue);
					}

					if (upperArmNoActivePos != null && target != null)
					{
						Debug.DrawLine(upperArmNoActivePos.position, target.position, Color.red);
					}
				}
			}
			
		}

	}

	void OnDrawGizmos()
	{
		if (debug)
		{
			if (upperArm != null && elbow != null && hand != null && target != null && elbow != null)
			{
				Gizmos.color = Color.gray;
				Gizmos.DrawLine(upperArm.position, forearm.position);
				Gizmos.DrawLine(forearm.position, hand.position);
				Gizmos.color = Color.red;
				Gizmos.DrawLine(upperArm.position, target.position);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(forearm.position, elbow.position);
			}
		}
	}
}

[Serializable]
public class V3Curves
{
	public AnimationCurve xCurve;
	public AnimationCurve yCurve;
	public AnimationCurve zCurve;
}
