﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RampSelector : MonoBehaviour
{
    private Bike bikeScr;

    public List<GameObject> RampList;
    public int currentRamp = 0;

    // Start is called before the first frame update
    void Start()
    {
        bikeScr = LevelManager.Instance.bike.GetComponent<Bike>();
        ChangeRamp(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Period))
        {
            NextRamp();
        }
        if (Input.GetKeyDown(KeyCode.Comma))
        {
            PrevRamp();
        }
    }

    public void NextRamp()
    {
        currentRamp = (currentRamp + 1) % RampList.Count;
        ChangeRamp(currentRamp);
    }

    public void PrevRamp()
    {
        currentRamp = (currentRamp - 1) % RampList.Count;
        if (currentRamp < 0) currentRamp += RampList.Count;
        ChangeRamp(currentRamp);
    }

    public void ChangeRamp(int index)
    {
        int currentIndex = 0;
        foreach (var ramp in RampList)
        {
            if (currentIndex == index)
                ramp.SetActive(true);
            else
                ramp.SetActive(false);
            currentIndex++;
        }

        bikeScr?.GetComponent<BikeBaseInput>()?.ResetPosition();
    }
    

}
