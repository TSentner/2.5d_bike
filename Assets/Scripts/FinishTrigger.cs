﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTrigger : MonoBehaviour
{
    private Bike bikeScr;

    private void Start()
    {
        bikeScr = LevelManager.Instance.bike.GetComponent<Bike>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!bikeScr)
            return;

        if (other.CompareTag("Player"))
        {
            other.transform.position = LevelManager.Instance.startPoint.position;
            bikeScr.StartBikeRecovery();
        }
    }
}
