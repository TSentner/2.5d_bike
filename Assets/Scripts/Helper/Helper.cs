﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Helper : MonoBehaviour
{
    public static void Setter(Action<float> action, float val, float newVal, UnityEvent<float> e, string nameVal)
    {
        //we compare fractional taking into account the error
        //so we get rid of the loop if in UnityEvent 
        //the assignment of this property will be caused again
        if (Mathf.Approximately(val, newVal))
            return;

        if (nameVal != "")
            PlayerPrefs.SetFloat(nameVal, newVal);
        action(newVal);
        e?.Invoke(newVal);
    }


    public static void Setter(Action<bool> action, bool val, bool newVal, UnityEvent<bool> e, string nameVal)
    {
        //we compare fractional taking into account the error
        //so we get rid of the loop if in UnityEvent 
        //the assignment of this property will be caused again
        if (val == newVal)
            return;

        if (nameVal != "")
            PlayerPrefs.SetInt(nameVal, newVal ? 1 : 0);
        action(newVal);
        e?.Invoke(newVal);
    }
}
