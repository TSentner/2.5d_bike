﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtension
{
    public static bool Approximately(this float v1, float v2, float allowedDifference)
    {
        var dif = v1 - v2;
        if (Mathf.Abs(dif) > allowedDifference)
            return false;

        return true;
    }

    public static bool Approximately(this Vector3 v1, Vector3 v2, float allowedDifference)
    {
        var dif = v1.x - v2.x;
        if (Mathf.Abs(dif) > allowedDifference)
            return false;

        dif = v1.y - v2.y;
        if (Mathf.Abs(dif) > allowedDifference)
            return false;

        dif = v1.z - v2.z;
        if (Mathf.Abs(dif) > allowedDifference)
            return false;

        return true;
    }
}
