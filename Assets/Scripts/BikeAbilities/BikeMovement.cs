﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class BikeMovement : BaseBikeAbility
{
    private BikeAnimationSystem _animationSystem;

    private float inputPushAxis;
    private bool gasPressed = false;
    private bool isPushActive = false;
    private bool IsPushActive
    {
        get => isPushActive;
        set {
            if (value == false)
            {
                isPushAnimStart = false;
            }

            if (isPushActive == value)
                return;

            isPushActive = value;


            if (value == false)
            {
                //StartRechargePush();
                isPushPhysicsActive = isPushAnimatorTriger = false;
            }
        }
    }

    private bool isPushAnimatorTriger = false;
    private bool isPushPhysicsActive = false;
    private bool isPushAnimStart = false;
    private bool isPushAnimEnd = false;


    private float idleDelay = 0.4f;
    private float? idleStartDelay = null;

    #region privateFootSlideVariables
    private Vector3 LastFootPos;
    private float footPositionScale = 1f;
    private float rechargeStartTime;
    private float timeStartPush;
    private float startSlideVelocity;
    private float oldCurrentTimePush;
    private float oldCurrentTimePushProgress;
    private float oldSTraveled;
    private float pushDirection;
    private float currentSlideMaxPushTime;
    #endregion

    public AnimationCurve curveSlide;

    public float minFootSlideTime = 0.1f;
    public float maxFootSlideTime = 3f;

    [SerializeField]
    private float lengthFootSlide = 1f; // the length of contact between foot and ground // in meters lengthFootSlide
    [SerializeField]
    private float maxFootSlideVelocity;
    [SerializeField]
    private float maxFootAccel = 25; //The speed after which the force is not given
    [SerializeField]
    private float rechargeTime = .5f;

    #region UIStats Properties and Methods used only for UI stats

    public UIStatManager.UIFloatStatData lengthFootSlideStatData;
    [HideInInspector]
    public UnityEvent<float> OnLengthFootSlideChange;
    
    public float LengthFootSlide
    {
        get => lengthFootSlide;
        set => Helper.Setter(val => lengthFootSlide = val, lengthFootSlide, value, OnLengthFootSlideChange, "LengthFootSlide");
    }

    public UIStatManager.UIFloatStatData maxFootSlideVelocityStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxFootSlideVelocityChange;

    public float MaxFootSlideVelocity
    {
        get => lengthFootSlide / minFootSlideTime;
        set => Helper.Setter(val => minFootSlideTime = lengthFootSlide / val, lengthFootSlide / minFootSlideTime, value, OnMaxFootSlideVelocityChange, "MaxFootSlideVelocity");
    }

    public UIStatManager.UIFloatStatData maxFootAccelStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxFootAccelChange;
    
    public float MaxFootAccel
    {
        get => maxFootAccel;
        set => Helper.Setter(val => maxFootAccel = val, maxFootAccel, value, OnMaxFootAccelChange, "MaxFootAccel");
    }

    public UIStatManager.UIFloatStatData rechargeTimeStatData;
    [HideInInspector]
    public UnityEvent<float> OnRechargeTimeChange;

    public float RechargeTime
    {
        get => rechargeTime;
        set => Helper.Setter(val => rechargeTime = val, rechargeTime, value, OnRechargeTimeChange, "RechargeTime");
    }

    protected override void DrowUIObjects()
    {
        base.DrowUIObjects();

        uiStatManager.DrowUIObject(lengthFootSlideStatData);
        uiStatManager.DrowUIObject(maxFootSlideVelocityStatData);
        uiStatManager.DrowUIObject(maxFootAccelStatData);
        uiStatManager.DrowUIObject(rechargeTimeStatData, true);
    }

    protected override void SubscribeUIOjects()
    {
        base.SubscribeUIOjects();

        OnLengthFootSlideChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnLengthFootSlideChange, lengthFootSlideStatData, LengthFootSlide, value => LengthFootSlide = value);
        OnMaxFootAccelChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxFootAccelChange, maxFootAccelStatData, MaxFootAccel, value => MaxFootAccel = value);
        OnRechargeTimeChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnRechargeTimeChange, rechargeTimeStatData, RechargeTime, value => RechargeTime = value);
        OnMaxFootSlideVelocityChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxFootSlideVelocityChange, maxFootSlideVelocityStatData,
                                        MaxFootSlideVelocity, value => MaxFootSlideVelocity = value);
    }
    #endregion

    [Header("Foot Position")]
    public Transform Foot;
    public Vector3 DefaultFootPos;
    public Vector3 IKFootOffset;


    protected override void Start()
    {
        base.Start();

        bikeScr._animationHandler.OnStartPush.AddListener(PushAnimatorStartPushTriger); 
        bikeScr._animationHandler.OnEndPush.AddListener(PushAnimatorEndPushTriger); 
        bikeScr._animationHandler.OnEndReturnHip.AddListener(PushAnimatorEndTriger);
         _animationSystem = bikeScr._animationSystem;
    }

    protected override void LoadPlayerPrefs()
    {
        base.LoadPlayerPrefs();

        if (PlayerPrefs.HasKey("LengthFootSlide"))
            LengthFootSlide = PlayerPrefs.GetFloat("LengthFootSlide");

        if (PlayerPrefs.HasKey("MaxFootSlideVelocity"))
            MaxFootSlideVelocity = PlayerPrefs.GetFloat("MaxFootSlideVelocity");

        if (PlayerPrefs.HasKey("MaxFootAccel"))
            MaxFootAccel = PlayerPrefs.GetFloat("MaxFootAccel");

        if (PlayerPrefs.HasKey("RechargeTime"))
            MaxFootAccel = PlayerPrefs.GetFloat("RechargeTime");
    }

    internal override void UpdateAbilities()
    {
        base.UpdateAbilities();

        GetInput();
        HandleMovement();
        AnimateRechargeTime();
        Gas();

    }

    private void HandleMovement()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal)
            return;

        //check if we just got grounded
        if (bikeScr.JustGotGrounded)
        {
            _movement.ChangeState(BikeStates.MovementStates.Idle);
        }

        

        // if we're grounded and moving, and currently Idle, we become Driving
        if (bikeScr.IsGrounded && !bikeScr.CheckIsHorizontalVelocityZero()
            && _movement.CurrentState == BikeStates.MovementStates.Idle
            )
        {
            _movement.ChangeState(BikeStates.MovementStates.Movement);
        }

        // if we're not moving and standing on the ground, we become Idle state
        if (bikeScr.IsGrounded && bikeScr.CheckIsHorizontalVelocityZero()
            && _movement.CurrentState == BikeStates.MovementStates.Movement
            )
        {
            bikeScr.rbBike.velocity = Vector3.up * bikeScr.rbBike.velocity.y;
            _movement.ChangeState(BikeStates.MovementStates.Idle);
        }

        // if the character is not grounded, but currently Idle or Driving, we change its state to Falling
        if (!bikeScr.IsGrounded
            && (
                (_movement.CurrentState == BikeStates.MovementStates.Movement)
                 || (_movement.CurrentState == BikeStates.MovementStates.Idle)
                ))
        {
            _movement.ChangeState(BikeStates.MovementStates.Falling);
        }
    }


    void AnimateRechargeTime()
    {
        if (Time.time - rechargeStartTime < RechargeTime)
        {
            var rechargeProgress = (Time.time - rechargeStartTime) / RechargeTime;
            ResetFootPosition(rechargeProgress);
            return;
        }
    }

    private void ResetFootPosition(float rechargeProgress)
    {
        Vector3 FootToCenter = DefaultFootPos - LastFootPos;
        Foot.localPosition = LastFootPos + FootToCenter.normalized * (FootToCenter.magnitude * rechargeProgress);
    }

    /// <summary>
    /// Motion physics
    /// </summary>
    private void Gas()
    {
        if (!bikeScr.IsWheelGrounded
            || Time.time - rechargeStartTime < RechargeTime //Check recharge time
            || _movement.CurrentState != BikeStates.MovementStates.Idle
               && _movement.CurrentState != BikeStates.MovementStates.Movement
            )
        {
            IsPushActive = false;
            
            if (!(Time.time - rechargeStartTime < RechargeTime))
                ResetFootPosition(1);
            return;
        }

        if (IsPushActive || !IsPushActive && !Mathf.Approximately(inputPushAxis, .0f))
        {
            isPushAnimStart = true;

            FootSlide();
        }
    }

    private void FootSlide()
    {
        if (!IsPushActive) //Just start push
        {

            IsPushActive = true;

            pushDirection = Mathf.Sign(inputPushAxis);
            oldCurrentTimePushProgress = -1f;
            oldSTraveled = .0f;
        }

        if (!isPushAnimatorTriger && !isPushPhysicsActive)
            return;

        CheckWakeUpWheels();

        //GameManager.Instance.logUI.text = "";
        Vector3 forwardVelocity = Vector3.Project(bikeScr.rbBike.velocity, bikeScr.rbBike.transform.forward); //velocity by forward vector
        //GameManager.Instance.logUI.text += "velocity " + forwardVelocity.magnitude.ToString();

        float currentVelocity = forwardVelocity.magnitude;

        float velocityDirection = Vector3.Dot(bikeScr.rbBike.transform.forward, forwardVelocity.normalized); //co-directionality or cos angle
        velocityDirection = Mathf.Sign(velocityDirection);
        // Check if we slow down
        bool isSlowDown = Mathf.Sign(velocityDirection * pushDirection) < .0f;

        if (isPushAnimatorTriger)
        {

            isPushPhysicsActive = true;
            isPushAnimatorTriger = false;


            timeStartPush = Time.time;
            startSlideVelocity = currentVelocity;

            if (isSlowDown)
                currentSlideMaxPushTime = maxFootSlideTime;
            else
                currentSlideMaxPushTime = Mathf.Clamp(LengthFootSlide / startSlideVelocity, minFootSlideTime, maxFootSlideTime);
        }

        float currentTimePush = Time.time - timeStartPush;

        float tOnePush = GetCurrentTimeOnePush(currentVelocity, currentTimePush);
        //if (Mathf.Approximately(tOnePush, minPushTime))
            //isFastPush = true;

        float currentTimePushProgress = (currentTimePush % tOnePush) / tOnePush;
        if (currentTimePushProgress < oldCurrentTimePushProgress || currentTimePushProgress >= 1f)
        { //Last slide step
            IsPushActive = false;
            //if (isFirstStep)
            //Foot.localPosition = new Vector3(0, -1, 3 * (-LengthFootSlide / 2 * pushDirection));
            //DoFastPush(currentVelocity, pushDirection);
            return;
        }
        else
        {
            var sTraveled = LengthFootSlide * currentTimePushProgress;

            SetFootPosition(sTraveled);

            float needVelocity = (sTraveled - oldSTraveled) / (currentTimePush - oldCurrentTimePush);

            float vDiff = needVelocity - currentVelocity;
            if (isSlowDown && vDiff < 0)
            {
                vDiff *= -1;
            }

            if (needVelocity > 0 && vDiff > 0)
            {
                float acceleration = vDiff / Time.deltaTime;
                if (acceleration > MaxFootAccel)
                    acceleration = MaxFootAccel;

                bikeScr.rbBike.AddForce(pushDirection * bikeScr.rbBike.transform.forward * acceleration, ForceMode.Acceleration);
                //var F = acceleration * bikeScr.MassBody; //mass correction
                //bikeScr.rbBike.AddForce(pushDirection * bikeScr.rbBike.transform.forward * F, ForceMode.Force);
                //Debug.Log(acceleration);
            }

            oldCurrentTimePush = currentTimePush;
            oldCurrentTimePushProgress = currentTimePushProgress;
            oldSTraveled = sTraveled;
        }
    }

    private float GetCurrentTimeOnePush(float currentVelocity, float currentTimePush)
    {
        float tOnePush;
        if (!Mathf.Approximately(currentVelocity, .0f))
        {
            float tDiff = currentSlideMaxPushTime - minFootSlideTime;
            if (tDiff > 0.00001f)
            {
                float tProgress = (tDiff - currentTimePush) / tDiff;
                tProgress = Mathf.Clamp01(tProgress);
                tProgress = curveSlide.Evaluate(tProgress);
                tOnePush = minFootSlideTime + tProgress * tDiff;
            }
            else
            {
                tOnePush = minFootSlideTime;
            }

            tOnePush = Mathf.Clamp(tOnePush, minFootSlideTime, currentSlideMaxPushTime);
        }
        else
        {
            tOnePush = maxFootSlideTime;
        }

        return tOnePush;
    }

    private void SetFootPosition(float sTraveled)
    {
        Vector3 start;
        float footY = .0f;
        if (pushDirection > .0f)
            footY = bikeScr.WMeshForward.localPosition.y - bikeScr.WColForward.radius;
        else
            footY = bikeScr.WMeshBack.localPosition.y - bikeScr.WColBack.radius;

        start = bikeScr.transform.TransformPoint(new Vector3(0, footY,
                                pushDirection * LengthFootSlide / 2 / bikeScr.transform.lossyScale.z) * footPositionScale);

        var footPos = start
            + sTraveled * footPositionScale * (-bikeScr.transform.forward) * Mathf.Sign(pushDirection)
            + IKFootOffset;
        Foot.position = footPos;
    }

    private void StartRechargePush()
    {
        rechargeStartTime = Time.time;
        LastFootPos = Foot.localPosition;
    }

    #region UslessFunctionsNow

    private void AddMovementForce(float currentConstForce)
    {
        currentConstForce = inputPushAxis * currentConstForce * Time.deltaTime;
        bikeScr.rbBike.AddForce(bikeScr.rbBike.transform.forward * currentConstForce, ForceMode.Force);
    }

    /*
    public AnimationCurve curveForce;
    private float constForce = 25; //Сonstant applied force


    private void DoFastPush(float currentVelocity, float direction)
    {
        float force;
        force = GetForceAccordingMaxVelocity(currentVelocity);
        Debug.Log("fastPush " + force);
        force = direction * force * Time.deltaTime;
        bikeScr.rbBike.AddForce(bikeScr.rbBike.transform.forward * force, ForceMode.Impulse);
    }

    
    private float GetForceAccordingMaxVelocity(float currentVelocity)
    {
        float progress = currentVelocity / MaxFootAccel; // percent of current velocity

        //return ConstForce * Easing.Quadratic.In(progress); // the closer to zero the slower 

        return ConstForce * (1 - curveForce.Evaluate(progress)); // MaxVelocity = zero acceleration
    }
    */

    #endregion

    /// <summary>
    /// In Idle state wheelСollider can "fall asleep"
    /// therefore wake them up
    /// </summary>
    private void CheckWakeUpWheels()
    {
        //WakeUp Wheels
        //if (-0.001f < bikeScr.rbBike.velocity.z && bikeScr.rbBike.velocity.z < 0.001f)
        if (_movement.CurrentState == BikeStates.MovementStates.Idle)
        //if (false)
        {
            bikeScr.SetSmallTorqueWCol(Mathf.Sign(inputPushAxis));
        }
    }

    private void GetInput()
    {
        if (gasPressed)
            inputPushAxis = 1;
        else
            inputPushAxis = Input.GetAxis("Horizontal");

        //bikeAccel *= bikeScr.BackToFront;
    }

    public void GasPressed()
    {
        gasPressed = true;
    }

    public void GasUnpressed()
    {
        gasPressed = false;
    }

    #region SubscribeBikeMovement

    protected override void SubscribeBikeMovement()
    {
        base.SubscribeBikeMovement();


        _movement.AddListener(BikeStates.MovementStates.Idle, StartIdleAnimation);
        _movement.AddFinishListener(BikeStates.MovementStates.Idle, FinishIdleAnimation);

        _movement.AddListener(BikeStates.MovementStates.Movement, StartMovementAnimation);
        _movement.AddFinishListener(BikeStates.MovementStates.Movement, FinishMovementAnimation);
    }

    void StartIdleAnimation()
    {
        idleStartDelay = Time.time;
    }

    void FinishIdleAnimation()
    {
        idleStartDelay = null;
    }


    void StartMovementAnimation()
    {

    }

    void FinishMovementAnimation()
    {

    }
    #endregion

    private void PushAnimatorStartPushTriger()
    {
        isPushAnimatorTriger = true;
        IsPushActive = true;
    }

    private void PushAnimatorEndPushTriger()
    {
        IsPushActive = false;
    }


    private void PushAnimatorEndTriger()
    {
        isPushAnimEnd = true;
    }

    internal override void UpdateAnimator()
    {
        base.UpdateAnimator();

        if (_movement.CurrentState == BikeStates.MovementStates.Idle)
        {
            if (idleStartDelay != null
            && Time.time - idleStartDelay < idleDelay)
            {
                _animationSystem.animator.SetBool("Idle", false);
                _animationSystem.animator.SetBool("Movement", true);
            }
            else
            {
                _animationSystem.animator.SetBool("Idle", true);
                _animationSystem.animator.SetBool("Movement", false);
                idleStartDelay = null;
            }
        }
        else
        {
            _animationSystem.animator.SetBool("Idle", false);
            _animationSystem.animator.SetBool("Movement", _movement.CurrentState == BikeStates.MovementStates.Movement);
        }

        if (_movement.CurrentState == BikeStates.MovementStates.Movement
            || _movement.CurrentState == BikeStates.MovementStates.Idle
            )
        {
            if (!Mathf.Approximately(inputPushAxis, .0f))
                isPushAnimStart = true;

            if (isPushAnimEnd)
            {
                if (!isPushAnimStart)
                {
                    _animationSystem.animator.SetBool("Push", false);
                }
                isPushAnimEnd = false;
            }

            if (isPushAnimStart)
            {
                _animationSystem.animator.SetBool("Push", true);
                isPushAnimStart = false;
            }
        }
        else
        {
            IsPushActive = false;
            _animationSystem.animator.SetBool("Push", false);
        }


    }
    
}
