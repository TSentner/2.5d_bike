﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bike))]
public class BaseBikeAbility : MonoBehaviour
{
    protected Bike bikeScr;
    protected UIStatManager uiStatManager;

    protected BikeStateMachine<BikeStates.MovementStates> _movement;
    protected BikeStateMachine<BikeStates.BikeConditions> _condition;

    public bool AbilityInitialized { get; private set; }

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        bikeScr = GetComponent<Bike>();
        _movement = bikeScr.MovementState;
        _condition = bikeScr.ConditionState;
        uiStatManager = LevelManager.Instance.uiStatManager;

        if (uiStatManager)
        {
            DrowUIObjects();
            SubscribeUIOjects();
        }

        SubscribeBikeCondition();
        SubscribeBikeMovement();

        LoadPlayerPrefs();

        AbilityInitialized = true;
    }

    protected virtual void LoadPlayerPrefs()
    {
        
    }

    protected virtual void DrowUIObjects()
    {

    }

    protected virtual void SubscribeUIOjects()
    {

    }

    internal virtual void UpdateAbilities()
    {

    }

    internal virtual void UpdateAnimator()
    {

    }

    protected virtual void SubscribeBikeMovement()
    {

    }

    protected virtual void SubscribeBikeCondition()
    {

    }
}
