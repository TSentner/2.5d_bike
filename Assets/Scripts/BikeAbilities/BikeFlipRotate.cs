﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BikeFlipRotate : BaseBikeAbility
{
    private Vector3 bufCenterOfMass;
    private float currentAngleSpeed;
    private bool isAngularDragChange = false;
    private float bufAngularDrag = .0f;

    public float flipAngularDrag; //Changes angularDrap parameter in rigidbody,  if conditions for a flip met
    public Transform RotatePivot; //Transform around which we rotate Bike

    [SerializeField]
    private float maxAngleSpeed = 7;
    [SerializeField]
    private float anglePerSecond = 20;
    [SerializeField]
    private float angleDrag = 25;
    [SerializeField]
    private float centerY = 2.8f;

    [Range(10, 90), SerializeField]
    private float maxAccelerometrAngle = 45;

    #region UIStats Properties and Methods used only for UI stats

    public UIStatManager.UIFloatStatData maxAngleSpeedStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxAngleSpeedChange;

    public float MaxAngleSpeed
    {
        get => maxAngleSpeed;
        set => Helper.Setter(val => maxAngleSpeed = val, maxAngleSpeed, value, OnMaxAngleSpeedChange, "MaxAngleSpeed");
    }

    public UIStatManager.UIFloatStatData anglePerSecondStatData;
    [HideInInspector]
    public UnityEvent<float> OnAnglePerSecondChange;

    public float AnglePerSecond
    {
        get => anglePerSecond;
        set => Helper.Setter(val => anglePerSecond = val, anglePerSecond, value, OnAnglePerSecondChange, "AnglePerSecond");
    }

    public UIStatManager.UIFloatStatData angleDragStatData;
    [HideInInspector]
    public UnityEvent<float> OnAngleDragChange;

    public float AngleDrag
    {
        get => angleDrag;
        set => Helper.Setter(val => angleDrag = val, angleDrag, value, OnAngleDragChange, "AngleDrag");
    }

    public UIStatManager.UIFloatStatData centerYStatData;
    [HideInInspector]
    public UnityEvent<float> OnCenterYChange;

    public float CenterY
    {
        get => centerY;
        set => Helper.Setter(val => {
            if (RotatePivot)
                RotatePivot.localPosition = new Vector3(RotatePivot.localPosition.x, val, RotatePivot.localPosition.z); 
            centerY = val;
        }, centerY, value, OnCenterYChange, "CenterY");
    }

    public UIStatManager.UIFloatStatData maxAccelerometrAngleStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxAccelerometrAngleChange;

    public float MaxAccelerometrAngle
    {
        get => maxAccelerometrAngle;
        set => Helper.Setter(val => maxAccelerometrAngle = val, maxAccelerometrAngle, value, OnMaxAccelerometrAngleChange, "MaxAccelerometrAngle");
    }


    protected override void LoadPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("MaxAngleSpeed"))
            MaxAngleSpeed = PlayerPrefs.GetFloat("MaxAngleSpeed");

        if (PlayerPrefs.HasKey("AnglePerSecond"))
            AnglePerSecond = PlayerPrefs.GetFloat("AnglePerSecond");

        if (PlayerPrefs.HasKey("AngleDrag"))
            AngleDrag = PlayerPrefs.GetFloat("AngleDrag");

        if (PlayerPrefs.HasKey("CenterY"))
            CenterY = PlayerPrefs.GetFloat("CenterY");

        if (PlayerPrefs.HasKey("MaxAccelerometrAngle"))
            MaxAccelerometrAngle = PlayerPrefs.GetFloat("MaxAccelerometrAngle");
    }

    protected override void DrowUIObjects()
    {
        base.DrowUIObjects();

        uiStatManager.DrowUIObject(maxAngleSpeedStatData);
        uiStatManager.DrowUIObject(anglePerSecondStatData);
        uiStatManager.DrowUIObject(angleDragStatData);
        uiStatManager.DrowUIObject(centerYStatData);
        uiStatManager.DrowUIObject(maxAccelerometrAngleStatData, true);
    }

    protected override void SubscribeUIOjects()
    {
        base.SubscribeUIOjects();

        OnMaxAngleSpeedChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxAngleSpeedChange, maxAngleSpeedStatData, MaxAngleSpeed, value => MaxAngleSpeed = value);
        OnMaxAccelerometrAngleChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxAccelerometrAngleChange, maxAccelerometrAngleStatData, MaxAccelerometrAngle, value => MaxAccelerometrAngle = value);
        OnAnglePerSecondChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnAnglePerSecondChange, anglePerSecondStatData, AnglePerSecond, value => AnglePerSecond = value);
        OnAngleDragChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnAngleDragChange, angleDragStatData, AngleDrag, value => AngleDrag = value);
        OnCenterYChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnCenterYChange, centerYStatData, CenterY, value => CenterY = value);
    }

    #endregion

    private float rotateAccel;
    private float rotatePressed = 0;

    protected override void Start()
    {
        base.Start();

    }

    internal override void UpdateAbilities()
    {
        base.UpdateAbilities();

        GetInput();
    }

    private void FixedUpdate()
    {
        CheckAngularDrag();
        HandleFlipRotate();
        //ChangeCenterOfMass();
    }

    /// <summary>
    /// Changes angularDrap parameter in rigidbody, if conditions for a flip met
    /// </summary>
    private void CheckAngularDrag()
    {
        if (!isAngularDragChange && EvaluateFlipRotateConditions())
        {
            bufAngularDrag = bikeScr.rbBike.angularDrag;
            bikeScr.rbBike.angularDrag = flipAngularDrag;
            isAngularDragChange = true;
            return;
        }

        if (isAngularDragChange && !EvaluateFlipRotateConditions())
        {
            bikeScr.rbBike.angularDrag = bufAngularDrag;
            isAngularDragChange = false;
            return;
        }
    }

    private void ChangeCenterOfMass()
    {
        if (bufCenterOfMass.Approximately(Vector3.zero, 0.000001f))
        {
            bufCenterOfMass = bikeScr.CenterOfMass.localPosition;
        }

        if (bikeScr.JustOffGrounded)
        {
            bufCenterOfMass = bikeScr.CenterOfMass.localPosition;
            bikeScr.CenterOfMass.localPosition = new Vector3(0, 0.84f, 0);
        }
        

        if (bikeScr.IsGrounded)
            bikeScr.CenterOfMass.localPosition = bufCenterOfMass;
        //bikeScr.rbBike.angularVelocity = Vector3.zero;
    }

    /// <summary>
    /// Flip
    /// Rotation speed aiming for "currentAngleFlipSpeed = rotateAccel * anglePerSecond" limited +/-maxAngleFlipSpeed
    /// 
    /// The pursuit of currentAngleFlipSpeed is achieved by constantly acting resistance angleDrag
    /// AngleDrag depends on current speed. Max currentAngleFlipSpeed = Max AngleDrag
    /// </summary>
    private void HandleFlipRotate()
    {
        if (!EvaluateFlipRotateConditions())
        {
            currentAngleSpeed = 0;
            return;
        }

        currentAngleSpeed += rotateAccel * AnglePerSecond * Time.fixedDeltaTime;
        currentAngleSpeed = Mathf.Clamp(currentAngleSpeed, -MaxAngleSpeed, MaxAngleSpeed);

        //Calculate resistance
        if (!Mathf.Approximately(currentAngleSpeed, .0f))
        {
            float currentAngleDrag = angleDrag * Mathf.Abs(currentAngleSpeed) / MaxAngleSpeed; // Drag according to current rotation speed
            float oldCurrent = currentAngleSpeed;
            currentAngleSpeed += -Mathf.Sign(currentAngleSpeed) * currentAngleDrag * Time.fixedDeltaTime;

            if (oldCurrent * currentAngleSpeed < .0f)
            {
                currentAngleSpeed = .0f;
            }
        }

        bikeScr.transform.RotateAround(RotatePivot.position, Vector3.right, currentAngleSpeed);
    }

    private bool EvaluateFlipRotateConditions()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || (
                _movement.CurrentState != BikeStates.MovementStates.Falling
                && _movement.CurrentState != BikeStates.MovementStates.Jumping
                && _movement.CurrentState != BikeStates.MovementStates.Spin
                && _movement.CurrentState != BikeStates.MovementStates.RolledOver
                )
            )
            return false;
        return true;
    }

    private void GetInput()
    {
        if (!Mathf.Approximately(rotatePressed, .0f))
            rotateAccel = rotatePressed;
        else if (!Mathf.Approximately(Input.GetAxis("FlipRotation"), .0f))
            rotateAccel = Input.GetAxis("FlipRotation");
        else
        {
            float signAccelX = Mathf.Sign(Input.acceleration.x);

            var maxAngle = MaxAccelerometrAngle / 90;
            var accelX = Mathf.Abs(Input.acceleration.x) / maxAngle;
            //accelX = Mathf.Clamp01(accelX);

            rotateAccel = signAccelX * Easing.Linear(accelX);
        }
    }

    public void RotatePressed(float val)
    {
        rotatePressed = val;
    }

    public void RotateUnpressed()
    {
        rotatePressed = .0f;
    }
}
