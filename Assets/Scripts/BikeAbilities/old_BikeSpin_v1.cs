﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Bike))]
public class old_BikeSpin_v1 : MonoBehaviour
{
    enum SpinType
    {
        Spin360,
        Spin180
    }

    private Bike bikeScr;
    private UIStatManager uiStatManager;

    private BikeStateMachine<BikeStates.MovementStates> _movement;
    private BikeStateMachine<BikeStates.BikeConditions> _condition;

    private Vector3 needRotationEuler;
    private float progressMultiBy2; // progress from -1 to 1
    private float progress = 0; // progress from 0 to 1
    private Quaternion startRotation;
    private Quaternion nextRotation;
    private BikeFixPositionRotation FixScr;
    private bool spinPressed;
    private bool spinRightPressed;
    private bool spinLeftPressed;
    private float currentSpeed = 0;
    private float currentSpeedProgress = 0;
    private readonly float dblClickTime = .25f;
    private float dblClickLastTime = .0f;
    private SpinType currentSpinType = new SpinType();
    private int currentSpinDirection = 1;
    [SerializeField]
    private float startSpeed = 2;
    [SerializeField]
    private float maxSpeed = 10;
    

    #region UIVariables

    public UIStatManager.UIFloatStatData startSpeedStatData;
    [HideInInspector]
    public UnityEvent<float> OnStartSpeedChange;
    public float StartSpeed
    {
        get => (currentSpinType == SpinType.Spin360) ? startSpeed / 2 : startSpeed;
        set => Helper.Setter(val => startSpeed = val, startSpeed, value, OnStartSpeedChange, "StartSpeed");
    }

    public UIStatManager.UIFloatStatData maxSpeedStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxSpeedChange;
    public float MaxSpeed
    {
        get => (currentSpinType == SpinType.Spin360) ? maxSpeed / 2 : maxSpeed;
        set => Helper.Setter(val => maxSpeed = val, maxSpeed, value, OnMaxSpeedChange, "MaxSpeed");
    }

    public UIStatManager.UIFloatStatData speedProgressAccelStatData;
    [HideInInspector]
    public UnityEvent<float> OnSpeedProgressAccelChange;
    public float SpeedProgressAccel
    {
        get => speedProgressAccel;
        set => Helper.Setter(val => speedProgressAccel = val, speedProgressAccel, value, OnSpeedProgressAccelChange, "SpeedProgressAccel");
    }
    #endregion

    [SerializeField]
    private float speedProgressAccel = .2f;


    private void Start()
    {
        bikeScr = GetComponent<Bike>();
        _movement = bikeScr.MovementState;
        _condition = bikeScr.ConditionState;

        if (uiStatManager)
        {
            uiStatManager = LevelManager.Instance.uiStatManager;
            uiStatManager.DrowUIObject(startSpeedStatData, true);
            uiStatManager.DrowUIObject(maxSpeedStatData, true);
            uiStatManager.DrowUIObject(speedProgressAccelStatData, true);
            SubscribeUIOjects();
        }

        FixScr = transform.GetComponent<BikeFixPositionRotation>();

        LoadPlayerPrefs();
    }

    private void LoadPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("StartSpeed"))
            StartSpeed = PlayerPrefs.GetFloat("StartSpeed");

        if (PlayerPrefs.HasKey("MaxSpeed"))
            MaxSpeed = PlayerPrefs.GetFloat("MaxSpeed");

        if (PlayerPrefs.HasKey("SpeedProgressAccel"))
            SpeedProgressAccel = PlayerPrefs.GetFloat("SpeedProgressAccel");
    }

    private void SubscribeUIOjects()
    {
        OnStartSpeedChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnStartSpeedChange, startSpeedStatData, StartSpeed, value => StartSpeed = value);

        OnMaxSpeedChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxSpeedChange, maxSpeedStatData, MaxSpeed, value => MaxSpeed = value);

        OnSpeedProgressAccelChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnSpeedProgressAccelChange, speedProgressAccelStatData, SpeedProgressAccel, value => SpeedProgressAccel = value);
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        Spin();
        CheckSpinBrake();
    }

    private void CheckSpinBrake()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState != BikeStates.MovementStates.Spin //not Spin
            )
            return;

        if (bikeScr.IsGrounded)
        {
            SpinBrake();
        }
    }

    private void SpinBrake()
    {
        SpinOver();
        bikeScr.ConditionState.ChangeState(BikeStates.BikeConditions.Recovery);
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("SpinLeft"))
            SpinLeftPressed();

        if (Input.GetButtonDown("SpinRight"))
            SpinRightPressed();

        if (Input.GetButtonUp("SpinLeft"))
            SpinLeftUnpressed();

        if (Input.GetButtonUp("SpinRight"))
            SpinRightUnpressed();

        if (spinPressed)
        {
            SpinAcceleration();
            DoSpin();
        }

        if(!spinPressed || _movement.CurrentState != BikeStates.MovementStates.Spin)
        {
            ResetSpinAcceleration();
        }
    }

    private void ResetSpinAcceleration()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState == BikeStates.MovementStates.Spin
            )
            return;

        currentSpeedProgress = 0;
    }

    private void SpinAcceleration()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState != BikeStates.MovementStates.Spin
            )
            return;

        currentSpeedProgress += SpeedProgressAccel * Time.deltaTime;
        currentSpeedProgress = Mathf.Clamp01(currentSpeedProgress);
        currentSpeed = StartSpeed + (MaxSpeed - StartSpeed) * Easing.Quadratic.Out(currentSpeedProgress);
    }

    private void Freeze(bool isFreeze)
    {
        FixScr?.Freeze(isFreeze);
    }

    public void DoSpin()
    {

        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState == BikeStates.MovementStates.Spin
            || ( _movement.CurrentState != BikeStates.MovementStates.Falling
                && _movement.CurrentState != BikeStates.MovementStates.Jumping
                )
            )
            return;

        _movement.ChangeState(BikeStates.MovementStates.Spin);

        Freeze(false);

        progressMultiBy2 = 1;
        progress = 0;
        nextRotation = startRotation = transform.rotation.normalized;
    }

    private void SpinOver()
    {
        Freeze(true);
        _movement.ChangeState(BikeStates.MovementStates.Falling);//Spin is Over
    }

    private void Spin()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState != BikeStates.MovementStates.Spin
            )
            return;

        Vector3 nextEulerRot;
        bool isSpinOver = GetNextEulerRotation(out nextEulerRot);

        Quaternion pitch = Quaternion.AngleAxis(nextEulerRot.x, Vector3.right);
        Quaternion roll = Quaternion.AngleAxis(nextEulerRot.y, Vector3.up);
        Quaternion yaw = Quaternion.AngleAxis(nextEulerRot.z, Vector3.forward);

        //Check if transform has been rotated.
        Quaternion previousRotation = nextRotation;
        float angle = Quaternion.Angle(transform.rotation, previousRotation);
        if (!Mathf.Approximately(angle, .0f)) //offset occurred
        {
            //a * b = c; a = c * !b; b = c * !a
            //a = previousRotation; c = transform.rotation;
            Quaternion offsetQuat = transform.rotation * Quaternion.Inverse(previousRotation);
            startRotation *= offsetQuat;
            startRotation.Normalize();
        }

        nextRotation = startRotation * pitch * roll * yaw;
        transform.rotation = nextRotation;

        if (isSpinOver)
        {
            /*
            Vector3 targetForward = bikeScr.transform.localRotation * Vector3.forward;
            float directionForward = Vector3.Dot(targetForward, Vector3.forward);
            directionForward = Mathf.Sign(directionForward);

            Vector3 targetUp = bikeScr.transform.localRotation * Vector3.up;
            float directionUp = Vector3.Dot(targetUp, Vector3.up);
            directionUp = Mathf.Sign(directionUp);
            */

            Vector3 targetRight = bikeScr.transform.localRotation * Vector3.right;
            float directionRight = Vector3.Dot(targetRight, Vector3.right);
            directionRight = Mathf.Sign(directionRight);

            //which way is it more rotate
            if (directionRight > 0)
                bikeScr.BackToFront = 1;
            else
                bikeScr.BackToFront = -1;

            SpinOver();
        }
    }

    private bool GetNextEulerRotation(out Vector3 nextEulerRot)
    {
        float oldProgress = progress;
        progress += currentSpeed / 2 * Time.deltaTime;
        progress = progress % 1;

        progressMultiBy2 += currentSpeed * Time.deltaTime;
        progressMultiBy2 = progressMultiBy2 % 2;

        bool isFin = false;
        if (oldProgress > progress)
        {
            progress = 1;
            progressMultiBy2 = 1;
            isFin = true;
        }

        float progressPingPong = Mathf.Abs(progressMultiBy2 - 1);
        nextEulerRot = new Vector3(
                needRotationEuler.x * Easing.Cubic.Out(progressPingPong),
                needRotationEuler.y * progress,
                needRotationEuler.z * Easing.Quadratic.Out(progressPingPong)
            );

        return isFin;
    }

    public void SpinRightPressed()
    {
        spinRightPressed = true;
        SpinPressed(1);
    }

    public void SpinRightUnpressed()
    {
        spinRightPressed = false;
        if (spinLeftPressed)
            SpinLeftPressed();
        else
            spinPressed = false;
    }

    public void SpinLeftPressed()
    {
        spinLeftPressed = true;
        SpinPressed(-1);
    }

    public void SpinLeftUnpressed()
    {
        spinLeftPressed = false;
        if (spinRightPressed)
            SpinRightPressed();
        else
            spinPressed = false;
    }

    private void SpinPressed(int direction = 1)
    {
        if (Time.time - dblClickLastTime < dblClickTime) //dblClick
            SetSpinType(SpinType.Spin180, direction);
        else
            SetSpinType(SpinType.Spin360, direction);

        dblClickLastTime = Time.time;
        spinPressed = true;
    }

    private void SetSpinType(SpinType type, int direction = 1)
    {
        if (_movement.CurrentState == BikeStates.MovementStates.Spin
            && currentSpinDirection != direction) //if change direction
        {
            progress = 1 - progress;
            needRotationEuler *= -1;
            startRotation *= Quaternion.Euler(needRotationEuler);
        }

        currentSpinDirection = direction;

        switch (type)
        {
            case SpinType.Spin360:
                if (_movement.CurrentState == BikeStates.MovementStates.Spin) //360 can only be enabled at the beginning.
                    return;

                currentSpinType = type;
                needRotationEuler = new Vector3(0, direction*360, 0);

                break;
            case SpinType.Spin180:
                if (_movement.CurrentState == BikeStates.MovementStates.Spin //already doing spin.
                    && currentSpinType == SpinType.Spin360) //switching from 360
                {
                    if (progress > 0.5f)
                    {
                        startRotation *= Quaternion.AngleAxis(180,Vector3.up);
                    }
                    progress = progress % 0.5f * 2;
                }

                currentSpinType = type;
                needRotationEuler = new Vector3(0, direction * 180, 0);

                break;
            default:
                return;
        }
    }

    


}
