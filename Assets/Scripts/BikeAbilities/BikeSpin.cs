﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BikeSpin : BaseBikeAbility
{
    enum SpinType
    {
        Spin360,
        Spin180
    }

    private bool isSpinOver = true;
    private float needYRot;
    private float progress = 0; // progress from 0 to 1
    private float nextYRot;
    private BikeFixPositionRotation _fixScr;
    private bool spinPressed;
    private bool spinRightPressed;
    private bool spinLeftPressed;
    private bool spinOppoUIBtnPressed;
    private float spinOppoPressedLastTime = .0f;
    private readonly float spinOppoClickTime = .25f;
    private float currentSpeed = 0;
    private float currentSpeedProgress = 0;
    private readonly float dblClickTime = .25f;
    private float dblClickLastTime = .0f;
    private SpinType currentSpinType = new SpinType();
    private int currentSpinDirection = 1;

    public Text SpinText;

    [SerializeField]
    private float startSpeed = 2;
    [SerializeField]
    private float maxSpeed = 10;
    [SerializeField]
    private float speedProgressAccel = .2f;

    #region UIVariables

    public UIStatManager.UIFloatStatData startSpeedStatData;
    [HideInInspector]
    public UnityEvent<float> OnStartSpeedChange;
    public float StartSpeed
    {
        get => (currentSpinType == SpinType.Spin180) ? startSpeed * 2 : startSpeed;
        set => Helper.Setter(val => startSpeed = val, startSpeed, value, OnStartSpeedChange, "StartSpeed");
    }

    public UIStatManager.UIFloatStatData maxSpeedStatData;
    [HideInInspector]
    public UnityEvent<float> OnMaxSpeedChange;
    public float MaxSpeed
    {
        get => (currentSpinType == SpinType.Spin180) ? maxSpeed * 2 : maxSpeed;
        set => Helper.Setter(val => maxSpeed = val, maxSpeed, value, OnMaxSpeedChange, "MaxSpeed");
    }

    public UIStatManager.UIFloatStatData speedProgressAccelStatData;
    [HideInInspector]
    public UnityEvent<float> OnSpeedProgressAccelChange;
    public float SpeedProgressAccel
    {
        get => speedProgressAccel;
        set => Helper.Setter(val => speedProgressAccel = val, speedProgressAccel, value, OnSpeedProgressAccelChange, "SpeedProgressAccel");
    }
    #endregion

    protected override void Start()
    {
        base.Start();

        _fixScr = transform.GetComponent<BikeFixPositionRotation>();
    }

    protected override void LoadPlayerPrefs()
    {
        base.LoadPlayerPrefs();

        if (PlayerPrefs.HasKey("StartSpeed"))
            StartSpeed = PlayerPrefs.GetFloat("StartSpeed");

        if (PlayerPrefs.HasKey("MaxSpeed"))
            MaxSpeed = PlayerPrefs.GetFloat("MaxSpeed");

        if (PlayerPrefs.HasKey("SpeedProgressAccel"))
            SpeedProgressAccel = PlayerPrefs.GetFloat("SpeedProgressAccel");
    }

    protected override void DrowUIObjects()
    {
        base.DrowUIObjects();

        uiStatManager.DrowUIObject(startSpeedStatData);
        uiStatManager.DrowUIObject(maxSpeedStatData);
        uiStatManager.DrowUIObject(speedProgressAccelStatData, true);
    }

    protected override void SubscribeUIOjects()
    {
        base.SubscribeUIOjects();

        OnStartSpeedChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnStartSpeedChange, startSpeedStatData, StartSpeed, value => StartSpeed = value);

        OnMaxSpeedChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnMaxSpeedChange, maxSpeedStatData, MaxSpeed, value => MaxSpeed = value);

        OnSpeedProgressAccelChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnSpeedProgressAccelChange, speedProgressAccelStatData, SpeedProgressAccel, value => SpeedProgressAccel = value);
    }

    internal override void UpdateAbilities()
    {
        base.UpdateAbilities();

        GetInput();
        CheckSpinBrake();
    }

    private void FixedUpdate()
    {
        Spin();
    }

    private void CheckSpinBrake()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || isSpinOver && _movement.CurrentState != BikeStates.MovementStates.Spin //not Spin
            )
            return;

        if (!isSpinOver && bikeScr.IsGrounded)
        {
            SpinBrake();
        }
    }

    private void SpinBrake()
    {
        SpinOver();
        bikeScr.ConditionState.ChangeState(BikeStates.BikeConditions.Recovery);
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("SpinLeft"))
            SpinLeftPressed();

        if (Input.GetButtonDown("SpinRight"))
            SpinRightPressed();

        if (Input.GetButtonUp("SpinLeft"))
            SpinLeftUnpressed();

        if (Input.GetButtonUp("SpinRight"))
            SpinRightUnpressed();

        if (spinPressed)
        {
            SpinAcceleration();
            DoSpin();
        }

        if(!spinPressed || _movement.CurrentState != BikeStates.MovementStates.Spin)
        {
            ResetSpinAcceleration();
        }
    }

    private void ResetSpinAcceleration()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState == BikeStates.MovementStates.Spin
            )
            return;

        currentSpeedProgress = 0;
    }

    private void SpinAcceleration()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState != BikeStates.MovementStates.Spin
            )
            return;

        currentSpeedProgress += SpeedProgressAccel * Time.deltaTime;
        currentSpeedProgress = Mathf.Clamp01(currentSpeedProgress);
        currentSpeed = StartSpeed + (MaxSpeed - StartSpeed) * Easing.Quadratic.Out(currentSpeedProgress);
    }

    private void Freeze(bool isFreeze)
    {
        _fixScr?.Freeze(isFreeze);
    }

    public void DoSpin()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState == BikeStates.MovementStates.Spin
            || ( _movement.CurrentState != BikeStates.MovementStates.Falling
                && _movement.CurrentState != BikeStates.MovementStates.Jumping
                )
            )
            return;

        _movement.ChangeState(BikeStates.MovementStates.Spin);

        Freeze(false);

        isSpinOver = false;
        progress = 0;
        nextYRot = 0;
    }

    private void SpinOver()
    {
        Freeze(true);
        isSpinOver = true;
        _movement.ChangeState(BikeStates.MovementStates.Falling);//Spin is Over
    }

    private void Spin()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal
            || _movement.CurrentState != BikeStates.MovementStates.Spin
            )
            return;

        isSpinOver = NextProgressStep();

        float oldYRot = nextYRot;
        nextYRot = currentSpinDirection * needYRot * progress;
        float nextStepYRot = nextYRot - oldYRot;
        Quaternion roll = Quaternion.AngleAxis(nextStepYRot, Vector3.up);
        transform.rotation *= roll;

        if (isSpinOver)
        {
            Vector3 targetRight = bikeScr.transform.localRotation * Vector3.right;
            float directionRight = Vector3.Dot(targetRight, Vector3.right);
            directionRight = Mathf.Sign(directionRight);

            //which way is it more rotate
            if (directionRight > 0)
                bikeScr.BackToFront = 1;
            else
                bikeScr.BackToFront = -1;

            SpinOver();
        }
    }

    private bool NextProgressStep()
    {
        float oldProgress = progress;
        progress += currentSpeed * Time.fixedDeltaTime;
        progress = progress % 1;

        bool isFin = false;
        
        if (oldProgress > progress)
        {
            progress = 1;
            isFin = true;
        }

        return isFin;
    }

    public void SpinRightPressed()
    {
        spinRightPressed = true;
        SpinPressed(1);
    }

    public void SpinRightUnpressed()
    {
        spinRightPressed = false;
        if (spinLeftPressed)
            SpinLeftPressed();
        else
            spinPressed = false;
    }

    public void SpinLeftPressed()
    {
        spinLeftPressed = true;
        SpinPressed(-1);
    }

    public void SpinLeftUnpressed()
    {
        spinLeftPressed = false;
        if (spinRightPressed)
            SpinRightPressed();
        else
            spinPressed = false;
    }

    private void SpinPressed(int direction = 1)
    {
        if (Time.time - dblClickLastTime < dblClickTime) //dblClick
            SetSpinType(SpinType.Spin180, direction);
        else
            SetSpinType(SpinType.Spin360, direction);

        dblClickLastTime = Time.time;
        spinPressed = true;
    }

    private void SetSpinType(SpinType type, int direction = 1)
    {
        if (currentSpinDirection != direction)
        {
            progress = 1 - progress;
            nextYRot = direction * needYRot * progress;

        }
        currentSpinDirection = direction;

        switch (type)
        {
            case SpinType.Spin360:
                if (_movement.CurrentState == BikeStates.MovementStates.Spin) //360 can only be enabled at the beginning.
                    return;

                currentSpinType = type;
                needYRot = 360;
                break;
            case SpinType.Spin180:

                needYRot = 180;
                if (_movement.CurrentState == BikeStates.MovementStates.Spin //already doing spin.
                    && currentSpinType == SpinType.Spin360) //switching from 360
                {
                    progress = progress % 0.5f * 2;
                    nextYRot = direction * needYRot * progress;
                }

                currentSpinType = type;
                break;
            default:
                return;
        }
    }

    public void SpinUIBtnPressed()
    {
        if (spinOppoUIBtnPressed)
            SpinPressed(-1);
        else
            SpinPressed();
    }

    public void SpinUIBtnUnpressed()
    {
        spinPressed = false;
    }

    public void SpinOppoUIBtnPressed()
    {
        if (Time.time - spinOppoPressedLastTime < spinOppoClickTime) //fast click
        {
            spinOppoUIBtnPressed = !spinOppoUIBtnPressed;

            SpinText.color = spinOppoUIBtnPressed ? new Color(0xFF, 0xFF, 0x00) : Color.white;
        }

        spinOppoPressedLastTime = Time.time;
    }
}
