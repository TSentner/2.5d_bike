﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class BikeJump : BaseBikeAbility
{
    [SerializeField]
    private float jumpForce = 700;

    #region UIStats Properties and Methods used only for UI stats

    public UIStatManager.UIFloatStatData jumpStatData;

    [HideInInspector]
    public UnityEvent<float> OnJumpForceChange;

    public float JumpForce
    {
        get => jumpForce;
        set => Helper.Setter(val => jumpForce = val, jumpForce, value, OnJumpForceChange, "JumpForce");
    }

    protected override void LoadPlayerPrefs()
    {
        base.LoadPlayerPrefs();

        if (PlayerPrefs.HasKey("JumpForce"))
            JumpForce = PlayerPrefs.GetFloat("JumpForce");
    }

    protected override void DrowUIObjects()
    {
        base.DrowUIObjects();

        uiStatManager.DrowUIObject(jumpStatData, true);
    }

    protected override void SubscribeUIOjects()
    {
        base.SubscribeUIOjects();

        OnJumpForceChange = new UIStatManager.MyFloatEvent();
        uiStatManager.SubscribeUIOject(OnJumpForceChange, jumpStatData, JumpForce, value => JumpForce = value);
    }

    #endregion

    protected override void Start()
    {
        base.Start();

    }

    internal override void UpdateAbilities()
    {
        base.UpdateAbilities();

        GetInput();
        HandleJumping();
    }

    private void HandleJumping()
    {
        if (_condition.CurrentState != BikeStates.BikeConditions.Normal)
            return;

        // if we're is not on the ground and move down after the jump, we become Falling
        if (!bikeScr.IsGrounded 
            && bikeScr.rbBike.velocity.y < .0f 
            && _movement.CurrentState == BikeStates.MovementStates.Jumping)
        {
            _movement.ChangeState(BikeStates.MovementStates.Falling);
        }

        if (bikeScr.IsGrounded
            && bikeScr.rbBike.velocity.y < .01f
            && _movement.CurrentState == BikeStates.MovementStates.Jumping)
        {
            if (bikeScr.CheckIsHorizontalVelocityZero())
                _movement.ChangeState(BikeStates.MovementStates.Idle);
            else
                _movement.ChangeState(BikeStates.MovementStates.Movement);
        }
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
    }

    public void Jump()
    {
        if (!EvaluateJumpConditions())
            return;

        _movement.ChangeState(BikeStates.MovementStates.Jumping);

        bikeScr.rbBike.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
    }

    private bool EvaluateJumpConditions()
    {
        if(_condition.CurrentState != BikeStates.BikeConditions.Normal //state not normal
           || !bikeScr.IsWheelGrounded) //if no wheel touches the ground
        {
            return false;
        }
        return true;
    }
}
