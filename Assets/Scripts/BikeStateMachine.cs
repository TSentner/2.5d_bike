﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BikeStateMachine<T> where T : struct, IComparable, IConvertible, IFormattable
{ 
    public T CurrentState { get; protected set; }
    public T PreviousState { get; protected set; }

    private Dictionary<T, UnityEvent> conditionEventList;
    private Dictionary<T, UnityEvent> conditionFinishEventList;

    public BikeStateMachine()
    {
        conditionEventList = new Dictionary<T, UnityEvent>();
        conditionFinishEventList = new Dictionary<T, UnityEvent>();
    }

    public void AddListener(T typeConditions, UnityAction uAcion)
    {
        if (!conditionEventList.ContainsKey(typeConditions))
            conditionEventList[typeConditions] = new UnityEvent();

        conditionEventList[typeConditions].RemoveListener(uAcion); //only unique
        conditionEventList[typeConditions].AddListener(uAcion);
    }

    public void AddFinishListener(T typeConditions, UnityAction uAcion)
    {
        if (!conditionFinishEventList.ContainsKey(typeConditions))
            conditionFinishEventList[typeConditions] = new UnityEvent();

        conditionFinishEventList[typeConditions].RemoveListener(uAcion); //only unique
        conditionFinishEventList[typeConditions].AddListener(uAcion);
    }

    public void RemoveListener(T typeConditions, UnityAction uAcion)
    {
        if (!conditionEventList.ContainsKey(typeConditions))
            return;

        conditionEventList[typeConditions].RemoveListener(uAcion);
    }

    public void RemoveFinishListener(T typeConditions, UnityAction uAcion)
    {
        if (!conditionFinishEventList.ContainsKey(typeConditions))
            return;

        conditionFinishEventList[typeConditions].RemoveListener(uAcion);
    }

    public void ChangeState(T newState)
    {
        // if the "new state" is the current one, we do nothing and exit
        if (newState.Equals(CurrentState))
            return;

        // store our previous character movement state
        PreviousState = CurrentState;

        if (conditionFinishEventList.ContainsKey(CurrentState))
            conditionFinishEventList[CurrentState].Invoke();

        CurrentState = newState;
        //Debug.Log(newState.ToString());
        if (conditionEventList.ContainsKey(newState))
            conditionEventList[newState].Invoke();
    }

    /// <summary>
    /// Returns the character to the state it was in before its current state
    /// </summary>
    public void RestorePreviousState()
    {
        if (conditionFinishEventList.ContainsKey(CurrentState))
            conditionFinishEventList[CurrentState].Invoke();

        // we restore our previous state
        CurrentState = PreviousState;

        if (conditionEventList.ContainsKey(PreviousState))
            conditionEventList[PreviousState].Invoke();
    }
}
